<!doctype html>
<html lang="en">
<body style="background-color:#999da0;">
<!-- <?php  //include_once 'header.php'; ?> -->

<!--Search-->
<div id="search">
  <button type="button" class="close">×</button>
  <form>
    <input type="search" value="" placeholder="Search here...."  required/>
    <button type="submit" class="btn btn_common blue">Search</button>
  </form>
</div>

<section id="contact" class="padding">
  <div class="container">

    <div class="row padding-bottom" style="font-family: 'Source Sans Pro';">
    <center>  <div class="col-md-6 contact_address heading_space wow fadeInLeft" data-wow-delay="300ms">
<!--Log In -->
        <center><h2 class="heading heading_space">Enter In! <span class="divider-left"></span></h2></center>
        <form class="form-inline findus" id="login-form" method="post" style="width:70%;color:white;" action="<?php echo base_url();?>login_handler/setUserLoginSession"> 
          <div class="form-group">
                <input type="email" class="form-control" placeholder="Email" name="loginEmail" id="loginEmail" required>
                <input type="password" class="form-control" placeholder="Password" name="loginpwd" id="loginpwd" required>
                <a href="#" style="float:right; color:black;">Forgot your password?</a>
                <p><input type="submit" class="btn_common red border_radius" style="color:white;" name="btn_login" value="Log In"></p>
          </div>
        </form>
      </div></center>
<!--Register -->
      <center><div class="col-md-6 wow fadeInRight" data-wow-delay="300ms">
        <h2 class="heading heading_space">Register Yourself Now!<span class="divider-left"></span></h2>
        <form class="form-inline findus" id="signup-form" method="post" style="width:70%;color:white;">
            <div class="form-group">
                <input type="text" class="form-control" placeholder="Name"  name="firstName" id="firstName" required>
                <input type="email" class="form-control" placeholder="Email" name="email" id="email" required>
                <input type="password" class="form-control" placeholder="Password" name="pwd" id="pwd" required>
                <input type="password" class="form-control" placeholder="Re-enter Password" name="repwd" id="repwd" required>
                <p align="left"><b>Register as:<t></b>
                                              <input type="radio" name="registerAs" value="1" checked> Student
                                              <input type="radio" name="registerAs" value="2"> Expert
                                              <input type="radio" name="registerAs" value="3"> University
                                              </p>
                <input type="text" class="form-control" placeholder="City" name="city" id="city">
                <input type="text" class="form-control" placeholder="Phone number" name="pNumber" id="pNumber">
                <center><input type="submit" class="btn_common red border_radius" id="signup" name="signup" value="Register"></center>
            </div>
        </form>
  </div></center>
</div>
</section>

<!-- <script src="js/jquery-2.2.3.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/bootsnav.js"></script>
<script src="js/jquery.appear.js"></script>
<script src="js/jquery-countTo.js"></script>
<script src="js/jquery.parallax-1.1.3.js"></script>
<script src="js/owl.carousel.min.js"></script>
<script src="js/jquery.cubeportfolio.min.js"></script>
<script src="js/jquery.themepunch.tools.min.js"></script>
<script src="js/jquery.themepunch.revolution.min.js"></script>
<script src="js/revolution.extension.layeranimation.min.js"></script>
<script src="js/revolution.extension.navigation.min.js"></script>
<script src="js/revolution.extension.parallax.min.js"></script>
<script src="js/revolution.extension.slideanims.min.js"></script>
<script src="js/revolution.extension.video.min.js"></script>
<script src="js/wow.min.js"></script>
<script src="js/functions.js"></script> -->

</body>
</html>
