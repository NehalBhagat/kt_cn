<!doctype html>
<html lang="en">
  <!--FOOTER-->
  <footer class="padding-top">
    <div class="container">
      <div class="row">
        <div class="col-md-4 col-sm-4 footer_panel bottom25">
          <h3 class="heading bottom25">About Us<span class="divider-left"></span></h3>
          <a href="<?php echo base_url();?>home/index"><img src="<?php echo base_url();?>assets/images/logo-white.png" alt="logo" class="logo logo-display" style="width: 280px; height: 47px; "></a>
          <p>KnowledgeTime offers live online courses by best experts</p>
          <ul class="social_icon top25">
            <li><a href="https://www.facebook.com/knowledgetime.org/" class="facebook"><i class="fa fa-facebook"></i></a></li>
            <li><a href="https://twitter.com/knowledge_time" class="twitter"><i class="icon-twitter4"></i></a></li>
          </ul>
        </div>
        <div class="col-md-4 col-sm-4 footer_panel bottom25">
          <h3 class="heading bottom25">Quick Links<span class="divider-left"></span></h3>
          <ul class="links">
                    <li><a href="<?php echo base_url();?>home/index"><i class="icon-chevron-small-right"></i>Home</a></li>
                    <li><a href="<?php echo base_url();?>public/public_ci/knowledgeTimeTeam"><i class="icon-chevron-small-right"></i>Team</a></li>
                    <li><a href="<?php echo base_url();?>public/public_ci/gallery"><i class="icon-chevron-small-right"></i>Gallery</a></li>
                  <li><a href="<?php echo base_url();?>public/public_ci/certifications"><i class="icon-chevron-small-right"></i>Certifications</a></li>
                    <li><a href="<?php echo base_url();?>public/public_ci/knowledgeTimeFaq"><i class="icon-chevron-small-right"></i>FAQ</a></li>
                    <li><a href="<?php echo base_url();?>public/public_ci/privacypolicy"><i class="icon-chevron-small-right"></i>Privacy Policy</a></li>
          </ul>
        </div>
        <div class="col-md-4 col-sm-4 footer_panel bottom25">
          <h3 class="heading bottom25">Keep in Touch <span class="divider-left"></span></h3>
          <p class=" address"><i class="icon-map-pin"></i>Montreal, Canada</p>
          <p class=" address"><i class="icon-phone"></i>(514)-692-9467</p>
          <p class=" address"><i class="icon-mail"></i><a href="mailto:Edua@info.com">contact@knowledgetime.org</a></p>
          <img src="<?php echo base_url();?>assets/images/footer-map.png" alt="we are here" class="img-responsive">
        </div>
      </div>
    </div>
  </footer>
  <div class="copyright">
    <div class="container">
      <div class="row">
        <div class="col-md-12 text-center">
          <p>Copyright &copy; 2017 <a href="<?php echo base_url();?>home/index">KnowledgeTime</a>. all rights reserved.</p>
        </div>
      </div>
    </div>
  </div>
  <!--FOOTER ends-->

  <script src="<?php echo base_url();?>assets/js/jquery-2.2.3.js"></script>
  <script src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>
  <script src="<?php echo base_url();?>assets/js/bootsnav.js"></script>
  <script src="<?php echo base_url();?>assets/js/jquery.appear.js"></script>
  <script src="<?php echo base_url();?>assets/js/jquery-countTo.js"></script>
  <script src="<?php echo base_url();?>assets/js/jquery.parallax-1.1.3.js"></script>
  <script src="<?php echo base_url();?>assets/js/owl.carousel.min.js"></script>
  <script src="<?php echo base_url();?>assets/js/jquery.cubeportfolio.min.js"></script>
  <script src="<?php echo base_url();?>assets/js/jquery.themepunch.tools.min.js"></script>
  <script src="<?php echo base_url();?>assets/js/jquery.themepunch.revolution.min.js"></script>
  <script src="<?php echo base_url();?>assets/js/revolution.extension.layeranimation.min.js"></script>
  <script src="<?php echo base_url();?>assets/js/revolution.extension.navigation.min.js"></script>
  <script src="<?php echo base_url();?>assets/js/revolution.extension.parallax.min.js"></script>
  <script src="<?php echo base_url();?>assets/js/revolution.extension.slideanims.min.js"></script>
  <script src="<?php echo base_url();?>assets/js/revolution.extension.video.min.js"></script>
  <script src="<?php echo base_url();?>assets/js/wow.min.js"></script>
  <script src="<?php echo base_url();?>assets/js/functions.js"></script>

  </html>
