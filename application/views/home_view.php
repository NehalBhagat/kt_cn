
<!--Slider-->
<section class="rev_slider_wrapper text-center">
<!-- START REVOLUTION SLIDER 5.0 auto mode -->
  <div id="rev_slider" class="rev_slider"  data-version="5.0">
    <ul>
    <!-- SLIDE  -->
      <li data-transition="fade">
        <!-- MAIN IMAGE -->
        <img src="<?php echo base_url();?>assets/images/banner1.jpg" alt="" data-bgposition="center center" data-bgfit="cover" data-bgparallax="10" class="rev-slidebg">
        <!-- LAYER NR. 1 -->
        <div class="tp-caption tp-resizeme"
        data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']"
        data-y="['326','270','270','150']" data-voffset="['0','0','0','0']"
        data-responsive_offset="on"
        data-visibility="['on','on','on','on']"
        data-transform_idle="o:1;"
        data-transform_in="z:0;rX:0;rY:0;rZ:0;sX:0.9;sY:0.9;skX:0;skY:0;opacity:0;s:1500;e:Power3.easeInOut;"
        data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;"
        data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;"
        data-start="800"><h1>Live Online Learning</h1>
        </div>
        <!--<div class="tp-caption tp-resizeme"
        data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']"
        data-y="['380','340','300','350']" data-voffset="['0','0','0','0']"
        data-responsive_offset="on"
        data-visibility="['on','on','off','off']"
        data-transform_idle="o:1;"
        data-transform_in="opacity:0;s:1000;e:Power2.easeInOut;"
        data-transform_out="opacity:0;s:1000;s:1000;"
      data-start="1500"><p>Learn the cutting edge courses live<br/> by world's best passionate Industry and Academic experts</p>
    </div>-->
        <div class="tp-caption  tp-resizeme"
        data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']"
        data-y="['400','360','320','370']" data-voffset="['0','0','0','0']"
        data-responsive_offset="on"
        data-visibility="['on','on','on','on']"
        data-transform_idle="o:1;"
        data-transform_in="y:[-200%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;s:1500;e:Power3.easeInOut;"
        data-transform_out="auto:auto;s:1000;e:Power3.easeInOut;"
        data-mask_in="x:0px;y:0px;s:inherit;e:inherit;"
        data-mask_out="x:0;y:0;s:inherit;e:inherit;"
        data-start="2000">
      <!--  <a href="#." class="border_radius btn_common white_border">our services</a>
      -->  <a href="<?php echo base_url();?>courses" class="border_radius btn_common blue">View Courses</a>
        </div>

      </li>

      <li data-transition="fade">
        <img src="<?php echo base_url();?>assets/images/banner2.jpg"  alt="" data-bgposition="center center" data-bgfit="cover" data-bgparallax="10" class="rev-slidebg">
        <div class="tp-caption tp-resizeme"
        data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']"
        data-y="['326','270','270','150']" data-voffset="['0','0','0','0']"
        data-responsive_offset="on"
        data-visibility="['on','on','on','on']"
        data-transform_idle="o:1;"
        data-transform_in="z:0;rX:0;rY:0;rZ:0;sX:0.9;sY:0.9;skX:0;skY:0;opacity:0;s:1500;e:Power3.easeInOut;"
        data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;"
        data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;"
        data-start="800"><h1>Take the first step towards the success</h1>
        </div>
      <!--  <div class="tp-caption tp-resizeme"
        data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']"
        data-y="['380','340','300','350']" data-voffset="['0','0','0','0']"
        data-responsive_offset="on"
        data-visibility="['on','on','off','off']"
        data-transform_idle="o:1;"
        data-transform_in="opacity:0;s:1000;e:Power2.easeInOut;"
        data-transform_out="opacity:0;s:1000;s:1000;"
        data-start="1500"><p>Enjoy knowledge transfer by <br/> passionate experts </p>
      </div>-->
        <div class="tp-caption  tp-resizeme"
        data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']"
        data-y="['400','360','320','370']" data-voffset="['0','0','0','0']"
        data-responsive_offset="on"
        data-visibility="['on','on','on','on']"
        data-transform_idle="o:1;"
        data-transform_in="y:[-200%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;s:1500;e:Power3.easeInOut;"
        data-transform_out="auto:auto;s:1000;e:Power3.easeInOut;"
        data-mask_in="x:0px;y:0px;s:inherit;e:inherit;"
        data-mask_out="x:0;y:0;s:inherit;e:inherit;"
        data-start="2000">
        <a href="<?php echo base_url();?>courses" class="border_radius btn_common blue">View Courses</a>
      </div>
      </li>
    </ul>
  </div><!-- END REVOLUTION SLIDER -->
</section>


<!--ABout US-->
<section id="about" class="padding">
  <div class="container">
    <div class="row">
    <div class="icon_wrap padding-bottom-half clearfix">
      <div class="col-sm-4 icon_box text-center heading_space wow fadeInUp" data-wow-delay="300ms">
         <i class="icon-icons9"></i>
         <h4 class="text-capitalize bottom20 margin10">Live online courses</h4>
         <p class="no_bottom">Learn the latest trends and skills live from the best industry and academic experts.</p>
      </div>
      <div class="col-sm-4 icon_box text-center heading_space wow fadeInUp" data-wow-delay="400ms">
         <i class="icon-icons9"></i>
         <h4 class="text-capitalize bottom20 margin10">Best Industry Leaders</h4>
         <p class="no_bottom">Opportunity to learn the cutting edge technologies from the best industry epxerts.</p>
      </div>
      <div class="col-sm-4 icon_box text-center heading_space wow fadeInUp" data-wow-delay="500ms">
         <i class="icon-icons20"></i>
         <h4 class="text-capitalize bottom20 margin10">Research and Innovative learning</h4>
         <p class="no_bottom">Understand the future of subjects and master your skill from the renowed researchers</p>
      </div>
      <div class="col-sm-4 icon_box text-center heading_space wow fadeInUp" data-wow-delay="600ms">
         <i class="icon-globe"></i>
         <h4 class="text-capitalize bottom20 margin10">Projects by experts</h4>
         <p class="no_bottom">Involve in the practical projects and get guidance by experts</p>
      </div>
      <div class="col-sm-4 icon_box text-center heading_space wow fadeInUp" data-wow-delay="400ms">
         <i class="icon-layers"></i>
         <h4 class="text-capitalize bottom20 margin10">Cutting edge technologies</h4>
         <p class="no_bottom">Get the knowledge of cutting edge technologies from best experts</p>
      </div>
      <div class="col-sm-4 icon_box text-center heading_space wow fadeInUp" data-wow-delay="500ms">
         <i class="icon-laptop"></i>
         <h4 class="text-capitalize bottom20 margin10">Discussion with experts</h4>
         <p class="no_bottom">Opportunity to discuss with Industry and academic experts</p>
      </div>
      </div>
    </div>
  </div>
  <div class="container margin_top">
    <div class="row">
      <div class="col-md-7 col-sm-6 priorty wow fadeInLeft" data-wow-delay="300ms">
        <h2 class="heading bottom25">Welcome to KnowledgeTime<span class="divider-left"></span></h2>
        <p class="half_space">Welcome to the world of live online learning</p>
        <div class="row">
          <div class="col-md-6">
            <div class="about-post">
            <a href="excellent_experts.php" class="border_radius"><img src="<?php echo base_url();?>assets/images/hands.png" alt="hands"></a>
            <h4>Excellent Experts</h4>
            <p>Learn from world's best experts</p>
            </div>
            <div class="about-post">
            <a href="live_online_learning.php" class="border_radius"><img src="<?php echo base_url();?>assets/images/awesome.png" alt="hands"></a>
            <h4>Live Online Learning</h4>
            <p>Learn live at your time</p>
          </div>
          </div>
          <div class="col-md-6">
            <div class="about-post">
            <a href="expert_evaluation.php" class="border_radius"><img src="<?php echo base_url();?>assets/images/maintenance.png" alt="hands"></a>
            <h4>Expert Evaluation</h4>
            <p>Evaluation by Experts in domain </p>
            </div>
          <div class="about-post">
            <a href="projects.php" class="border_radius"><img src="<?php echo base_url();?>assets/images/home.png" alt="hands"></a>
            <h4>Projects</h4>
            <p>Involve in practical project</p>
            </div>
          </div>
        </div>
      </div>
      <div class="col-md-5 col-sm-6 wow fadeInRight">
        <div class "container" >

        <iframe style="width:100%; height:300px;" src="https://www.youtube.com/embed/CyZCglGLCCw" frameborder="0" allowfullscreen="0"></iframe>

            </div>
          </div>
    </div>
  </div>
</section>
<!--ABout US-->


<!-- Courses -->
<section id="courses" class="padding parallax">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <h2 class="heading heading_space wow fadeInDown">Popular Courses<span class="divider-left"></span></h2>
      </div>
    </div>
    <div class="row">
      <div class="col-md-12">
        <div class="slider_wrapper">
          <div id="course_slider" class="owl-carousel">
            <div class="item">
              <div class="image bottom20">
                <img src="<?php echo base_url();?>assets/images/courses/python.jpeg" alt="Courses" class="img-responsive border_radius">
              </div>
              <h3 class="bottom15"><a href="python_programming.php">Python Programming</a></h3>
      <!--        <p class="bottom15">We offer the most complete house renovating services in the country, from kitchen design to bathroom remodeling.</p> -->
              <a href="python_programming.php" class="btn_common blue border_radius">View Details</a>
            </div>
            <div class="item">
              <div class="image bottom20">
                <img src="<?php echo base_url();?>assets/images/courses/control_to_space.jpeg" alt="Courses" class="img-responsive border_radius">
              </div>
              <h3 class="bottom15"><a href="advanced_control_engineering.php">Advanced Control Engineering</a></h3>
          <!--    <p class="bottom15">We offer the most complete house renovating services in the country, from kitchen design to bathroom remodeling.</p>  -->
              <a href="advanced_control_engineering.php" class="btn_common blue border_radius">View Details</a>
            </div>
            <div class="item">
              <div class="image bottom20">
                <img src="<?php echo base_url();?>assets/images/courses/electronics_product_design.jpg" alt="Courses" class="img-responsive border_radius">
              </div>
              <h3 class="bottom15"><a href="electronics_product_design.php">Electronic Product Design</a></h3>
            <!--  <p class="bottom15">We offer the most complete house renovating services in the country, from kitchen design to bathroom remodeling.</p> -->
              <a href="electronics_product_design.php" class="btn_common blue border_radius">View Details</a>
            </div>
            <div class="item">
              <div class="image bottom20">
                <img src="<?php echo base_url();?>assets/images/courses/robotics.jpg" alt="Courses" class="img-responsive border_radius">
              </div>
              <h3 class="bottom15"><a href="robotics.php">Robotics</a></h3>
          <!--    <p class="bottom15">We offer the most complete house renovating services in the country, from kitchen design to bathroom remodeling.</p> -->
              <a href="robotics.php" class="btn_common blue border_radius">View Details</a>
            </div>
            <div class="item">
              <div class="image bottom20">
                <img src="<?php echo base_url();?>assets/images/courses/data_analysis.jpeg" alt="Courses" class="img-responsive border_radius">
              </div>
              <h3 class="bottom15"><a href="data_analysis.php">Data Analysis</a></h3>
            <!--  <p class="bottom15">We offer the most complete house renovating services in the country, from kitchen design to bathroom remodeling.</p> -->
              <a href="data_analysis.php" class="btn_common blue border_radius">View Details</a>
            </div>
            <div class="item">
              <div class="image bottom20">
                <img src="<?php echo base_url();?>assets/images/courses/internet_of_things.jpg" alt="Courses" class="img-responsive border_radius">
              </div>
              <h3 class="bottom15"><a href="internet_of_things.php">Internet of Things</a></h3>
          <!--    <p class="bottom15">We offer the most complete house renovating services in the country, from kitchen design to bathroom remodeling.</p> -->
              <a href="internet_of_things.php" class="btn_common blue border_radius">View Details</a>
            </div>
            <div class="item">
              <div class="image bottom20">
                <img src="<?php echo base_url();?>assets/images/courses/machine_learning.jpg" alt="Courses" class="img-responsive border_radius">
              </div>
              <h3 class="bottom15"><a href="machine_learning.php">Machine Learning</a></h3>
            <!--  <p class="bottom15">We offer the most complete house renovating services in the country, from kitchen design to bathroom remodeling.</p> -->
              <a href="machine_learning.php" class="btn_common blue border_radius">View Details</a>
            </div>
            <div class="item">
              <div class="image bottom20">
                <img src="<?php echo base_url();?>assets/images/courses/artificial_intelligence.jpeg" alt="Courses" class="img-responsive border_radius">
              </div>
              <h3 class="bottom15"><a href="artificial_intelligence.php">Artificial Intelligence</a></h3>
            <!--  <p class="bottom15">We offer the most complete house renovating services in the country, from kitchen design to bathroom remodeling.</p> -->
              <a href="artificial_intelligence.php" class="btn_common blue border_radius">View Details</a>
            </div>
            <div class="item">
              <div class="image bottom20">
                <img src="<?php echo base_url();?>assets/images/courses/artificial_intelligence.jpeg" alt="Courses" class="img-responsive border_radius">
              </div>
              <h3 class="bottom15"><a href="microfluidics.php">Microfluidics</a></h3>
          <!--    <p class="bottom15">We offer the most complete house renovating services in the country, from kitchen design to bathroom remodeling.</p> -->
              <a href="microfluidics.php" class="btn_common blue border_radius">View Details</a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<!-- Courses -->

<!--Fun Facts-->
<section id="facts" class="padding">
  <div class="container">
    <div class="row">
      <div class="col-md-12 text-center wow fadeInDown">
       <h2 class="heading">Live Online Learning<span class="divider-center"></span></h2>
       <p class="heading_space margin10"> Quality education for all</p>
      </div>
    </div>
    <div class="row number-counters">
      <div class="col-md-2 col-sm-4">
        <div class="counters-item">
        <i class="icon-checkmark3"></i>
        <strong data-to="1235">0</strong>
        <!-- Set Your Number here. i,e. data-to="56" -->
        <p>Project Completed</p>
        </div>
        <div class="counters-item last">
        <i class="icon-trophy"></i>
        <strong data-to="78">0</strong>
        <p>Awards Won</p>
        </div>
      </div>
      <div class="col-md-7 col-sm-4">
        <div class="fact-image">
        <img src="<?php echo base_url();?>assets/images/fun-facts.png" alt=" some facts" class="img-responsive">
        </div>
      </div>
      <div class="col-md-3 col-sm-4">
       <div class="counters-item">
        <i class=" icon-icons20"></i>
        <strong data-to="186">0</strong>
        <p>Hours of Work / Month</p>
        </div>
        <div class="counters-item last">
        <i class="icon-happy"></i>
        <strong data-to="89">0</strong>
        <p>Satisfied Clients</p>
        </div>
      </div>
    </div>
  </div>
</section>

<!--Customers Review-->
<section id="reviews" class="padding bg_light">
  <div class="container">
    <div class="row">
      <div class="col-md-12 text-center wow fadeInDown">
      <h2 class="heading heading_space">What People say <span class="divider-center"></span></h2>
      <div id="review_slider" class="owl-carousel text-center">
        <div class="item">
          <h4>Shubhashish Mehra</h4>
          <img src="<?php echo base_url();?>assets/images/customer1.jpg" class="client_pic border_radius" alt="costomer">
          <p>KnowledgeTime is a great innovative online educational platform. In today's fast pacing and competitive environment, this next generation way of learning was really missing but thankfully we have one now. The best part I like about knowledge time is its ability to combine the academics with industry and research to have the better understanding of any concept and get the best out of our time that too as it says at our own time, pace and space.</p>
        </div>
    <!--    <div class="item">
           <h4>apple</h4>
          <img src="images/customer1.png" class="client_pic border_radius" alt="costomer">
          <p>I've been happy with the services provided by Edua LLC. Scooter Libby has been wonderful! He has returned my calls quickly, and he answered all my questions. This is required when, for example, the final text is not yet available. We are here to help you from the initial phase to the final Edua phase.</p>
        </div>
        <div class="item">
           <h4>John Smith</h4>
          <img src="images/customer1.png" class="client_pic border_radius" alt="costomer">
          <p>I've been happy with the services provided by Edua LLC. Scooter Libby has been wonderful! He has returned my calls quickly, and he answered all my questions. This is required when, for example, the final text is not yet available. We are here to help you from the initial phase to the final Edua phase.</p>
        </div> -->
       </div>
      </div>
    </div>
  </div>
</section>




      <!--Pricings-->
<!--   <section class="padding" id="pricing">
  <div class="container">
    <div class="row">
      <div class="col-md-12 text-center wow fadeInDown">
        <h2 class="heading">Pricing Tables <span class="divider-center"></span></h2>
        <p class="heading_space margin10">Choose the suitable plan for you</p>
      </div>
      <div class="col-md-12">
        <div class="pricing">
          <div class="pricing_item wow fadeInUp" data-wow-delay="300ms">
            <h3>Basic</h3>
            <div class="pricing_price"><span class="pricing_currency">$</span>9.90</div>
            <p class="pricing_sentence">Perfect for users</p>
            <ul class="pricing_list">
              <li class="pricing_feature">Support forum</li>
              <li class="pricing_feature">Free hosting</li>
              <li class="pricing_feature">40MB of storage space</li>
              <li>Social media integration</li>
              <li>1GB of storage space</li>
            </ul>
            <a class="btn_common text-center" href="#.">Choose plan</a>
          </div>
          <div class="pricing_item active wow fadeInUp" data-wow-delay="400ms">
            <h3>Popular</h3>
            <div class="pricing_price"><span class="pricing_currency">$</span>29,90</div>
            <p class="pricing_sentence">Suitable for small businesses with up to 5 employees</p>
            <ul class="pricing_list">
              <li class="pricing_feature">Unlimited calls</li>
              <li class="pricing_feature">Free hosting</li>
              <li class="pricing_feature">10 hours of support</li>
              <li class="pricing_feature">Social media integration</li>
              <li class="pricing_feature">1GB of storage space</li>
            </ul>
            <a class="btn_common text-center" href="#.">Choose plan</a>
          </div>
          <div class="pricing_item dark_gray wow fadeInUp" data-wow-delay="500ms">
            <h3>Premier</h3>
            <div class="pricing_price"><span class="pricing_currency">$</span>59,90</div>
            <p class="pricing_sentence">Great for large businesses with more than 5 employees</p>
            <ul class="pricing_list">
              <li class="pricing_feature">Unlimited calls</li>
              <li class="pricing_feature">Free hosting</li>
              <li class="pricing_feature">Unlimited hours of support</li>
              <li class="pricing_feature">Social media integration</li>
              <li class="pricing_feature">Unlimited storage space</li>
            </ul>
            <a class="btn_common text-center" href="#.">Choose plan</a>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>    -->
<!--Pricings-->


<!--Paralax -->
<section id="parallax" class="parallax">
  <div class="container">
    <div class="row">
      <div class="col-md-12 text-center wow bounceIn">
       <h2>We Believe Albert Einstein's saying Wisdom is not a product of schooling but of the lifelong attempt to acquire it</h2>
       <h1 class="margin10">2016</h1>
       <a href="<?php echo base_url();?>public/public_ci/knowledgeTimeTeam" class="border_radius btn_common white_border margin10">KnowledgeTime</a>
      </div>
    </div>
  </div>
</section>
<!--Paralax -->


<!-- News-->
<section id="news" class="padding">
  <div class="container">
    <div class="row">
      <div class="col-md-12 wow fadeInDown">
       <h2 class="heading heading_space">Latest updates<span class="divider-left"></span></h2>
      </div>
    </div>
    <div class="row">
      <div class="col-md-12">
        <div class="slider_wrapper">
          <div id="news_slider" class="owl-carousel">
            <div class="item">
              <div class="content_wrap">
                <div class="image">
                  <img src="<?php echo base_url();?>assets/images/planets_7.jpg" alt="Edua" class="img-responsive border_radius">
                </div>
                <div class="news_box border_radius">
                  <h4><a href="https://www.nasa.gov/feature/jpl/earth-size-planets-the-newest-weirdest-generation">Earth-Size planets: The Newest, Weirdest Generation</a></h4>
                  <ul class="commment">
                    <li><a href="https://www.nasa.gov/feature/jpl/earth-size-planets-the-newest-weirdest-generation"><i class="icon-icons20"></i>Feb 23, 2017</a></li>
                    <li><a href="https://www.nasa.gov/feature/jpl/earth-size-planets-the-newest-weirdest-generation"><i class="icon-comment"></i></a></li>
                  </ul>
                  <p>Earth-Size Planets: The Newest, Weirdest Generation</p>
                  <a href="https://www.nasa.gov/feature/jpl/earth-size-planets-the-newest-weirdest-generation" class="readmore">Read More</a>
                </div>
              </div>
            </div>
            <div class="item">
              <div class="content_wrap">
                <div class="image">
                  <img src="<?php echo base_url();?>assets/images/planets_7.jpg" alt="Edua" class="img-responsive border_radius">
                </div>
                <div class="news_box border_radius">
                  <h4><a href="https://www.nasa.gov/feature/jpl/earth-size-planets-the-newest-weirdest-generation"> Earth-Size planets: The Newest, Weirdest Generation</a></h4>
                  <ul class="commment">
                    <li><a href="https://www.nasa.gov/feature/jpl/earth-size-planets-the-newest-weirdest-generation"><i class="icon-icons20"></i>Feb 23, 2017</a></li>
                    <li><a href="https://www.nasa.gov/feature/jpl/earth-size-planets-the-newest-weirdest-generation"><i class="icon-comment"></i> </a></li>
                  </ul>
                  <p>Earth-Size planets: The Newest, Weirdest Generation</p>
                  <a href="https://www.nasa.gov/feature/jpl/earth-size-planets-the-newest-weirdest-generation" class="readmore">Read More</a>
                </div>
              </div>
            </div>
            <div class="item">
              <div class="content_wrap">
                <div class="image">
                  <img src="<?php echo base_url();?>assets/images/planets_7.jpg" alt="Edua" class="img-responsive border_radius">
                </div>
                <div class="news_box border_radius">
                  <h4><a href="https://www.nasa.gov/feature/jpl/earth-size-planets-the-newest-weirdest-generation">Earth-Sized Planets:The Newest Weirdest Generation </a></h4>
                  <ul class="commment">
                    <li><a href="https://www.nasa.gov/feature/jpl/earth-size-planets-the-newest-weirdest-generation"><i class="icon-icons20"></i>Feb 23, 2017</a></li>
                    <li><a href="https://www.nasa.gov/feature/jpl/earth-size-planets-the-newest-weirdest-generation"><i class="icon-comment"></i></a></li>
                  </ul>
                  <p>Earth-Sized Planets: The Newest Weirdest Generation</p>
                  <a href="https://www.nasa.gov/feature/jpl/earth-size-planets-the-newest-weirdest-generation" class="readmore">Read More</a>
                </div>
              </div>
            </div>
      <!--      <div class="item">
              <div class="content_wrap">
                <div class="image">
                  <img src="images/news1.jpg" alt="Edua" class="img-responsive border_radius">
                </div>
                <div class="news_box border_radius">
                  <h4><a href="blog_detail.php">4 Springtime Color Schemes to Try at Home</a></h4>
                  <ul class="commment">
                    <li><a href="#."><i class="icon-icons20"></i>June 6, 2016</a></li>
                    <li><a href="#."><i class="icon-comment"></i> 02</a></li>
                  </ul>
                  <p>We offer the most complete house Services in the country...</p>
                  <a href="blog_detail.php" class="readmore">Read More</a>
                </div>
              </div>
            </div>  -->
        <!--    <div class="item">
              <div class="content_wrap">
                <div class="image">
                  <img src="images/news2.jpg" alt="Edua" class="img-responsive border_radius">
                </div>
                <div class="news_box border_radius">
                  <h4><a href="blog_detail.php">4 Springtime Color Schemes to Try at Home</a></h4>
                  <ul class="commment">
                    <li><a href="#."><i class="icon-icons20"></i>June 6, 2016</a></li>
                    <li><a href="#."><i class="icon-comment"></i> 02</a></li>
                  </ul>
                  <p>We offer the most complete house Services in the country...</p>
                  <a href="blog_detail.php" class="readmore">Read More</a>
                </div>
              </div>
            </div>  -->
        <!--    <div class="item">
              <div class="content_wrap">
                <div class="image">
                  <img src="images/news3.jpg" alt="Edua" class="img-responsive border_radius">
                </div>
                <div class="news_box border_radius">
                  <h4><a href="blog_detail.php">4 Springtime Color Schemes to Try at Home</a></h4>
                  <ul class="commment">
                    <li><a href="#."><i class="icon-icons20"></i>June 6, 2016</a></li>
                    <li><a href="#."><i class="icon-comment"></i> 02</a></li>
                  </ul>
                  <p>We offer the most complete house Services in the country...</p>
                  <a href="blog_detail.php" class="readmore">Read More</a>
                </div>
              </div>
            </div> -->
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
