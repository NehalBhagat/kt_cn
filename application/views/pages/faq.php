<!--Search-->
<div id="search">
  <button type="button" class="close">×</button>
  <form>
    <input type="search" value="" placeholder="Search here...."  required/>
    <button type="submit" class="btn btn_common blue">Search</button>
  </form>
</div>

<!--Page Header-->
<section class="page_header padding-top">
  <div class="container">
    <div class="row">
      <div class="col-md-12 page-content">
        <h1>FAQ’S</h1>
        <p>KnowledgeTime offers live online courses by best experts</p>
        <div class="page_nav">
      <span>You are here:</span> <a href="<?php echo base_url();?>home/index">Home</a> <span><i class="fa fa-angle-double-right"></i>FAQ’S</span>
      </div>
      </div>
    </div>
  </div>
</section>
<!--Page Header-->


<!--SERVICE SECTION-->
<section id="faq" class="padding">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
         <h2 class="heading heading_space wow fadeInDown">Frequently Asked Questions<span class="divider-left"></span></h2>
          <div class="faq_content wow fadeIn" data-wow-delay="400ms">
              <ul class="items">
                <li><a href="#." >How can I access and attend live courses?</a>
                  <ul class="sub-items">
                    <li>
                      <p>Currently our services are available to only university students.If you are university student and interested in our services, you can contact us, we will provide the service to your university. We are working on providing servies to non university students. </p>
                    </li>
                  </ul>
                </li>
                <li><a href="#.">Where KnowledgeTime's services are available?</a>
                  <ul class="sub-items">
                    <li><p>At present, we offer our services to all the engineering colleges and universities in India.</p>
                    </li>
                  </ul>
                </li>
                <li><a href="#.">I am an expert in particular domain, Is it possible to deliever live lecture in  KnowledgeTime</a>
                  <ul class="sub-items">
                    <li>
                      <p>Yes you can deliever the live lecture. For more details please contact us</p>
                    </li>
                  </ul>
                </li>
                <li><a href="#.">Does KnowledgeTime's services are available in Villages</a>
                  <ul class="sub-items">
                    <li>
                      <p>We are working on to provide our services to Villages in India.</p>
                    </li>
                  </ul>
                </li>
              </ul>
        </div>
      </div>
    </div>
  </div>
</section>
<div class="clearfix"></div>
