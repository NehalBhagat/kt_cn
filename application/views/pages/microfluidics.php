<?php session_start(); ?>
<!doctype html>
<html lang="en">
<body>
<?php include_once 'header.php'; ?>

<!--Search-->
<div id="search">
  <button type="button" class="close">×</button>
  <form>
    <input type="search" value="" placeholder="Search here...."  required/>
    <button type="submit" class="btn btn_common blue">Search</button>
  </form>
</div>


<!--Page Header-->
<section class="page_header padding-top">
  <div class="container">
    <div class="row">
      <div class="col-md-12 page-content">
        <h1>Student Courses</h1>
        <p>We offer the most complete house renovating services in the country</p>
        <div class="page_nav">
      <span>You are here:</span> <a href="index.php">Home</a> <span><i class="fa fa-angle-double-right"></i>Courses</span>
      </div>
      </div>
    </div>
  </div>
</section>
<!--Page Header-->


<!-- Courses -->
<section id="course_all" class="padding-bottom-half padding-top">
  <div class="container">
    <div class="row">
      <div class="col-sm-8 course_detail wow fadeIn" data-wow-delay="400ms">
        <img src="images/courses/microfluidics_details.jpg" alt="Course" class=" border_radius img-responsive bottom15">
        <div class="detail_course">
          <div class="info_label">
            <span class="icony"><i class="icon-users3"></i></span>
            <p>Teacher</p>
            <h5>John</h5>
          </div>
          <div class="info_label">
            <span class="icony"><i class="icon-users3"></i></span>
            <p>Category</p>
            <h5>Microfluidics</h5>
          </div>
          <div class="info_label hidden-xs"></div>
          <div class="info_label">
            <form class="star_rating bottom5">
              <div class="stars">
                <input type="radio" name="star" class="star-1" id="star-01" />
                <label class="star-1" for="star-01">1</label>
                <input type="radio" name="star" class="star-2" id="star-02" />
                <label class="star-2" for="star-02">2</label>
                <input type="radio" name="star" class="star-3" id="star-03" />
                <label class="star-3" for="star-03">3</label>
                <input type="radio" name="star" class="star-4" id="star-04" checked />
                <label class="star-4" for="star-04">4</label>
                <input type="radio" name="star" class="star-5"  id="star-05"  />
                <label class="star-5" for="star-05">5</label>
                <span></span>
              </div>
              <p class="no_bottom text-right">4 Rating</p>
            </form>

          </div>
        </div>
        <h3 class="top30 bottom20">Microfluidics</h3>
        <p class="bottom25">Microfluidics is the subject deals with the behavior, precise control and manipulation of fluids that are geometrically constrained to a small, typically sub-millimeter, scale. Here micro means the small volumes, small size, low energy consumption, effect of micro domain </p>
        <p class="bottom25">The subject has the applications in the </p>
                <div class="row">
          <div class="col-sm-6">
            <ul class="bullet_list">
              <li>Lab on Chip</li>
              <li>Inkjet Printhead</li>
              <li>DNA chips</li>
            </ul>
          </div>
          <div class="col-sm-6">
            <ul class="bullet_list">
              <li>Micropropulsion</li>
              <li>Healthcare Industry</li>
              <li>Basic Sciences</li>
            </ul>
          </div>
        </div>
        <p class="margin10 heading_space"> Microfluidics</p>
        <div class="bottom15"></div>
        <div class="profile_bg heading_space">
          <h3 class="bottom20">About the Expert</h3>
          <div class="profile">
            <div class="p_pic"><img src="images/profile1.png" alt="Course"></div>
            <div class="profile_text">
              <h5><strong>Satish I </strong>  -  <span>Mechanical Engineer</span></h5>
              <p>He has worked 5 years in the area of Bio-Microsystems. Currently he teaches microfluidics in  KnowledgeTime</p>
          <!--    <ul class="social_icon black top20">
                <li><a href="#." class="facebook"><i class="fa fa-facebook"></i></a></li>
                <li><a href="#." class="twitter"><i class="icon-twitter4"></i></a></li>
                <li><a href="#." class="dribble"><i class="icon-dribbble5"></i></a></li>
              </ul>    -->
            </div>
          </div>
        </div>
        <div class="reviews">
          <h3 class="bottom20">Review</h3>
          <div class="row">
            <div class="col-sm-4 heading_space">
              <p>Average Rating</p>
              <div class="review_left text-center">
                <strong class="bottom5">4.2</strong>
                <form class="star_rating bottom5">
                  <div class="stars">
                    <input type="radio" name="star" class="star-1" id="star-11" />
                    <label class="star-1" for="star-11">1</label>
                    <input type="radio" name="star" class="star-2" id="star-12" />
                    <label class="star-2" for="star-12">2</label>
                    <input type="radio" name="star" class="star-3" id="star-13" />
                    <label class="star-3" for="star-13">3</label>
                    <input type="radio" name="star" class="star-4" id="star-14" checked />
                    <label class="star-4" for="star-14">4</label>
                    <input type="radio" name="star" class="star-5"  id="star-15"  />
                    <label class="star-5" for="star-15">5</label>
                    <span></span>
                  </div>
                </form>
                <p class="no_bottom">4 Rating</p>
              </div>
            </div>
            <div class="col-sm-8 heading_space">
              <div class="rating_progress">
                <span>Stars 5</span>
                <div class="progress">
                  <div class="progress-bar" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width:80%;"></div>
                </div>
                <span>8</span>
                <div class="clearfix"></div>
                <span>Stars 4</span>
                <div class="progress">
                  <div class="progress-bar" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width:60%;"></div>
                </div>
                <span> 6</span>
                <div class="clearfix"></div>
                <span>Stars 3</span>
                <div class="progress">
                  <div class="progress-bar" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width:40%;"></div>
                </div>
                <span> 3</span>
                <div class="clearfix"></div>
                <span>Stars 2</span>
                <div class="progress">
                  <div class="progress-bar" role="progressbar" aria-valuenow="30" aria-valuemin="0" aria-valuemax="100" style="width:30%;"></div>
                </div>
                <span>1</span>
                <div class="clearfix"></div>
                <span>Stars 1</span>
                <div class="progress">
                  <div class="progress-bar" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width:0%;"></div>
                </div>
                <span> 0</span>
              </div>
            </div>
          </div>
        </div>
        <div class="profile_border">
          <div class="profile top20 bottom20">
            <div class="p_pic"><img src="images/profile2.png" alt="Course"></div>
            <div class="profile_text">
              <h5><strong>Nikhil Trivedi</strong>  -  <span> Great for Bio-Microsystems researchers</span></h5>
              <form class="star_rating bottom5">
                <div class="stars">
                  <input type="radio" name="star" class="star-1" id="star-21" />
                  <label class="star-1" for="star-21">1</label>
                  <input type="radio" name="star" class="star-2" id="star-22" />
                  <label class="star-2" for="star-22">2</label>
                  <input type="radio" name="star" class="star-3" id="star-23" />
                  <label class="star-3" for="star-23">3</label>
                  <input type="radio" name="star" class="star-4" id="star-24"  />
                  <label class="star-4" for="star-24">4</label>
                  <input type="radio" name="star" class="star-5"  id="star-25" checked  />
                  <label class="star-5" for="star-25">5</label>
                  <span></span>
                </div>
              </form>
              <p class="margin10">Professional Microsystem Engineer</p>
            </div>
          </div>
        </div>
        <div class="profile_border">
          <div class="profile top20 bottom20">
            <div class="p_pic"><img src="images/profile3.png" alt="Course"></div>
            <div class="profile_text">
              <h5><strong>Varsha P</strong>  -  <span>Beginner in microfabrication</span></h5>
              <form class="star_rating bottom5">
                <div class="stars">
                  <input type="radio" name="star" class="star-1" id="star-31" />
                  <label class="star-1" for="star-31">1</label>
                  <input type="radio" name="star" class="star-2" id="star-32" checked />
                  <label class="star-2" for="star-32">2</label>
                  <input type="radio" name="star" class="star-3" id="star-33" />
                  <label class="star-3" for="star-33">3</label>
                  <input type="radio" name="star" class="star-4" id="star-34" checked />
                  <label class="star-4" for="star-34">4</label>
                  <input type="radio" name="star" class="star-5"  id="star-35"  />
                  <label class="star-5" for="star-35">5</label>
                  <span></span>
                </div>
              </form>
              <p class="margin10">Its fun to understand the concepts</p>
            </div>
          </div>
        </div>
        <div class="profile_border heading_space">
          <div class="profile top20 bottom20">
            <div class="p_pic"><img src="images/profile4.png" alt="Course"></div>
            <div class="profile_text">
              <h5><strong>Pankaj S</strong>  -  <span>Awesome Quality</span></h5>
              <form class="star_rating bottom5">
                <div class="stars">
                  <input type="radio" name="star" class="star-1" id="star-41" />
                  <label class="star-1" for="star-41">1</label>
                  <input type="radio" name="star" class="star-2" id="star-42" />
                  <label class="star-2" for="star-42">2</label>
                  <input type="radio" name="star" class="star-3" id="star-43" />
                  <label class="star-3" for="star-43">3</label>
                  <input type="radio" name="star" class="star-4" id="star-44" checked />
                  <label class="star-4" for="star-44">4</label>
                  <input type="radio" name="star" class="star-5"  id="star-45" />
                  <label class="star-5" for="star-45">5</label>
                  <span></span>
                </div>
              </form>
              <p class="margin10">Beginner in Microsystems</p>
            </div>
          </div>
        </div>
        <h3>Add a Review </h3>
        <p class="heading_space">You must be <a href="#." class="logged">logged</a> in to post a comment.</p>
      </div>
      <aside class="col-sm-4 wow fadeIn" data-wow-delay="400ms">
        <div class="widget heading_space">
          <h3 class="bottom20">Featured Courses</h3>
          <div class="media">
            <a class="media-left" href="#."><img src="images/post1.png" alt="post"></a>
            <div class="media-body">
              <h5 class="bottom5">Artificial Intelligence</h5>
          <!--    <a href="#." class="btn-primary border_radius bottom5">free</a>  -->
              <form class="star_rating">
                <div class="stars">
                  <input type="radio" name="star" class="star-1" id="star-51" />
                  <label class="star-1" for="star-51">1</label>
                  <input type="radio" name="star" class="star-2" id="star-52" />
                  <label class="star-2" for="star-52">2</label>
                  <input type="radio" name="star" class="star-3" id="star-53" />
                  <label class="star-3" for="star-53">3</label>
                  <input type="radio" name="star" class="star-4" id="star-54"  />
                  <label class="star-4" for="star-54">4</label>
                  <input type="radio" name="star" class="star-5"  id="star-55" checked  />
                  <label class="star-5" for="star-55">5</label>
                  <span></span>
                </div>
              </form>
              <span class="name">Michael Windzor</span>
            </div>
          </div>
          <div class="media">
            <a class="media-left" href="#."><img src="images/post1.png" alt="post"></a>
            <div class="media-body">
              <h5 class="bottom5">Software Courses</h5>
        <!--      <a href="#." class="btn-primary border_radius bottom5">free</a> -->
              <form class="star_rating">
                <div class="stars">
                  <input type="radio" name="star" class="star-1" id="star-61" />
                  <label class="star-1" for="star-61">1</label>
                  <input type="radio" name="star" class="star-2" id="star-62" />
                  <label class="star-2" for="star-62">2</label>
                  <input type="radio" name="star" class="star-3" id="star-63" />
                  <label class="star-3" for="star-63">3</label>
                  <input type="radio" name="star" class="star-4" id="star-64"  checked />
                  <label class="star-4" for="star-64">4</label>
                  <input type="radio" name="star" class="star-5"  id="star-65"  />
                  <label class="star-5" for="star-65">5</label>
                  <span></span>
                </div>
              </form>
              <span class="name">Michael Windzor</span>
            </div>
          </div>
          <div class="media">
            <a class="media-left" href="#."><img src="images/post1.png" alt="post"></a>
            <div class="media-body">
              <h5 class="bottom5">Machine learning courses</h5>
        <!--      <a href="#." class="btn-primary border_radius bottom5">free</a>  -->
              <form class="star_rating">
                <div class="stars">
                  <input type="radio" name="star" class="star-1" id="star-71" />
                  <label class="star-1" for="star-71">1</label>
                  <input type="radio" name="star" class="star-2" id="star-72" />
                  <label class="star-2" for="star-72">2</label>
                  <input type="radio" name="star" class="star-3" id="star-73" />
                  <label class="star-3" for="star-73">3</label>
                  <input type="radio" name="star" class="star-4" id="star-74"  />
                  <label class="star-4" for="star-74">4</label>
                  <input type="radio" name="star" class="star-5"  id="star-75" checked  />
                  <label class="star-5" for="star-75">5</label>
                  <span></span>
                </div>
              </form>
              <span class="name">Michael Windzor</span>
            </div>
          </div>
        </div>
      <!--  <div class="widget heading_space">
          <h3 class="bottom20">Working Hours</h3>
          <p class="hours"> Monday <span>8:15 am - 5.30 pm</span></p>
          <p class="hours">Tuesday <span>8:15 am - 5.30 pm</span></p>
          <p class="hours">Wednesday <span>8:15 am - 5.30 pm</span></p>
          <p class="hours">Thursday <span>8:15 am - 5.30 pm</span></p>
          <p class="hours">Friday <span>8:15 am - 5.30 pm</span></p>
          <p class="hours">Saturday <span>9:30 am - 4.00 pm</span></p>
          <p class="hours">Sunday <span><a href="#." class="border_radius text-uppercase">closed</a></span></p>
        </div>
-->
        <div class="widget heading_space">
          <h3 class="bottom20">Top Tags</h3>
          <ul class="tags">
            <li><a href="#.">Books</a></li>
          <li><a href="#.">Midterm test </a></li>
            <li><a href="#.">Presentation</a></li>
            <li><a href="#.">Courses</a></li>
            <li><a href="#.">Certifications</a></li>
            <li><a href="#.">Teachers</a></li>
            <li><a href="#.">Student Life</a></li>
            <li><a href="#.">Study</a></li>
            <li><a href="#.">Midterm test </a></li>
            <li><a href="#.">Presentation</a></li>
            <li><a href="#.">Courses</a></li>
          </ul>
        </div>
      </aside>
    </div>
  </div>
</section>
<!-- Courses -->



<!--FOOTER-->
<?php include_once 'footer.php'; ?>
</body>
</html>
