<?php session_start(); ?>
<!doctype html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
<title>KnowledgeTime| Courses</title>
<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="css/font-awesome.min.css">
<link rel="stylesheet" type="text/css" href="css/edua-icons.css">
<link rel="stylesheet" type="text/css" href="css/animate.min.css">
<link rel="stylesheet" type="text/css" href="css/owl.carousel.css">
<link rel="stylesheet" type="text/css" href="css/owl.transitions.css">
<link rel="stylesheet" type="text/css" href="css/cubeportfolio.min.css">
<link rel="stylesheet" type="text/css" href="css/settings.css">
<link rel="stylesheet" type="text/css" href="css/bootsnav.css">
<link rel="stylesheet" type="text/css" href="css/style.css">
<link rel="stylesheet" type="text/css" href="css/loader.css">

<link rel="icon" href="images/favicon.png">

<!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->
</head>

<body>
<a href="#" class="scrollToTop"><i class="fa fa-angle-up"></i></a>
<!--Loader-->
<div class="loader">
  <div class="bouncybox">
      <div class="bouncy"></div>
    </div>
</div>

<div class="topbar">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="pull-left">
        <span class="info"><a href="#."> Website is under construction! Please contact for more details.</a></span>
        <span class="info"><i class="icon-phone2"></i>+1-514-692-9467</span>
        <span class="info"><i class="icon-mail"></i>contact@knowledgetime.org</span>
        </div>
        <ul class="social_top pull-right">
          <li><a href="https://www.facebook.com/knowledgetime.org/"><i class="fa fa-facebook"></i></a></li>
          <li><a href="https://twitter.com/knowledge_time"><i class="icon-twitter4"></i></a></li>
          <li><a href="#."><i class="icon-google"></i></a></li>
        </ul>
      </div>
    </div>
  </div>
</div>

<!--Header-->
<header>
  <nav class="navbar navbar-default navbar-sticky bootsnav">
    <div class="container">
       <div class="search_btn btn_common"><i class="icon-icons185"></i></div>
      <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-menu">
          <i class="fa fa-bars"></i>
        </button>
        <a class="navbar-brand" href="index.php">
          <img src="images/logo.png" class="logo logo-scrolled" alt style="  width: 280px; height: 47px; ">
        </a>
      </div>
      <div class="collapse navbar-collapse" id="navbar-menu">
        <ul class="nav navbar-nav navbar-right" data-in="fadeInDown" data-out="fadeOut">
          <li>
            <a href="index.php" class="dropdown-toggle" data-toggle="dropdown" >Home</a>
           </li>
          <li>
            <a href="courses.php" class="dropdown-toggle" data-toggle="dropdown" >courses</a>
            </li>
        <!--  <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" >events</a>
            <ul class="dropdown-menu">
              <li><a href="event.php">events</a></li>
              <li><a href="event_detail.php">Events Detail</a></li>
            </ul>
          </li>
-->
        <!--  <li class="dropdown megamenu-fw">-->
            <li>
            <a href="about.php" class="dropdown-toggle" data-toggle="dropdown">About</a>
          </li>
          <!--  <ul class="dropdown-menu megamenu-content" role="menu">
              <li>
                <div class="row">
                  <div class="col-menu col-md-3">
                    <h6 class="title">Pages</h6>
                    <div class="content">
                      <ul class="menu-col">
                        <li><a href="about.php">About</a></li>
                        <li><a href="testinomial.php">Testinomial</a></li>
                        <li><a href="our_team.php">Our team</a></li>
                  <li><a href="pricing.php">Pricings</a></li>
                      </ul>
                    </div>
                  </div>
                  <div class="col-menu col-md-3">
                    <h6 class="title">Blog</h6>
                    <div class="content">
                      <ul class="menu-col">
                        <li><a href="blog.php">Blog</a></li>
                        <li><a href="blog/blog2.php">Blog 02</a></li>
                        <li><a href="blog/blog3.php">Blog 03</a></li>
                        <li><a href="blog_detail.php">Blog Detail</a></li>
                      </ul>
                    </div>
                  </div>
                  <div class="col-menu col-md-3">
                    <h6 class="title">Shop</h6>
                    <div class="content">
                      <ul class="menu-col">
                        <li><a href="shop.php">Shop</a></li>
                        <li><a href="shop_detail.php">Shop Detail</a></li>
                        <li><a href="shop_cart.php">Cart</a></li>
                      </ul>
                    </div>
                  </div>
                  <div class="col-menu col-md-3">
                    <h6 class="title">Others</h6>
                    <div class="content">
                      <ul class="menu-col">
                        <li><a href="gallery.php">Gallery</a></li>
                        <li><a href="faq.php">Faq</a></li>
                        <li><a href="404.php">404</a></li>
                      </ul>
                    </div>
                  </div>
                </div>
              </li>
            </ul>
          </li>-->
          <li><a href="blog.php">Blog</a></li>
          <li><a href="contact.php">Contact</a></li>
          <li><a href="login.php">Login</a></li>
        </ul>
      </div>
    </div>
  </nav>
</header>


<!--Search-->
<div id="search">
  <button type="button" class="close">×</button>
  <form>
    <input type="search" value="" placeholder="Search here...."  required/>
    <button type="submit" class="btn btn_common blue">Search</button>
  </form>
</div>


<!--Page Header-->
<section class="page_header padding-top">
  <div class="container">
    <div class="row">
      <div class="col-md-12 page-content">
        <h1>Student Courses</h1>
        <p>We offer the most complete house renovating services in the country</p>
        <div class="page_nav">
      <span>You are here:</span> <a href="index.php">Home</a> <span><i class="fa fa-angle-double-right"></i>Courses</span>
      </div>
      </div>
    </div>
  </div>
</section>
<!--Page Header-->


<!-- Courses -->
<section id="course_all" class="padding-bottom-half padding-top">
  <div class="container">
    <div class="row">
      <div class="col-sm-8 course_detail wow fadeIn" data-wow-delay="400ms">
        <img src="images/python_detail.jpeg" alt="Course" class=" border_radius img-responsive bottom15">
        <div class="detail_course">
          <div class="info_label">
            <span class="icony"><i class="icon-users3"></i></span>
            <p>Teacher</p>
            <h5>John</h5>
          </div>
          <div class="info_label">
            <span class="icony"><i class="icon-users3"></i></span>
            <p>Category</p>
            <h5>Computer Science</h5>
          </div>
          <div class="info_label hidden-xs"></div>
          <div class="info_label">
            <form class="star_rating bottom5">
              <div class="stars">
                <input type="radio" name="star" class="star-1" id="star-01" />
                <label class="star-1" for="star-01">1</label>
                <input type="radio" name="star" class="star-2" id="star-02" />
                <label class="star-2" for="star-02">2</label>
                <input type="radio" name="star" class="star-3" id="star-03" />
                <label class="star-3" for="star-03">3</label>
                <input type="radio" name="star" class="star-4" id="star-04" checked />
                <label class="star-4" for="star-04">4</label>
                <input type="radio" name="star" class="star-5"  id="star-05"  />
                <label class="star-5" for="star-05">5</label>
                <span></span>
              </div>
              <p class="no_bottom text-right">4 Rating</p>
            </form>

          </div>
        </div>
        <h3 class="top30 bottom20">Python Programming</h3>
        <p class="bottom25">Python is high level programming language with the feature of object oriented programming.  Python supports modules and packages, which encourages program modularity and code reuse.
        </p>
        <p class="bottom25">The course involves the fundamentals of python programming. Data structures, databases etc. The course involves more examples, which helps you to understand the programming concepts much better. Passionate programmers show you how to build small applications using the python programming concepts. Industry experts demonstrates few applications. Python is used in many applications domains</p>
                <div class="row">
          <div class="col-sm-6">
            <ul class="bullet_list">
              <li>Web and Internet Development</li>
              <li>Scientific and Numeric</li>
              <li>Desktop GUIs</li>
            </ul>
          </div>
          <div class="col-sm-6">
            <ul class="bullet_list">
              <li>Software Development</li>
              <li>Web Applications Development</li>
              <li>Games Development</li>
            </ul>
          </div>
        </div>
        <p class="margin10 heading_space"> It is about the robotics course</p>
        <div class="bottom15"></div>
        <div class="profile_bg heading_space">
          <h3 class="bottom20">About the Expert</h3>
          <div class="profile">
            <div class="p_pic"><img src="images/profile1.png" alt="Course"></div>
            <div class="profile_text">
              <h5><strong>Satish Inamdar</strong>  -  <span>Python Programmer</span></h5>
              <p>He has worked 5 years in the area of Python programming. Currently he teaches python programming in  KnowledgeTime</p>
              <ul class="social_icon black top20">
                <li><a href="#." class="facebook"><i class="fa fa-facebook"></i></a></li>
                <li><a href="#." class="twitter"><i class="icon-twitter4"></i></a></li>
                <li><a href="#." class="dribble"><i class="icon-dribbble5"></i></a></li>
              </ul>
            </div>
          </div>
        </div>
        <div class="reviews">
          <h3 class="bottom20">Review</h3>
          <div class="row">
            <div class="col-sm-4 heading_space">
              <p>Average Rating</p>
              <div class="review_left text-center">
                <strong class="bottom5">4.2</strong>
                <form class="star_rating bottom5">
                  <div class="stars">
                    <input type="radio" name="star" class="star-1" id="star-11" />
                    <label class="star-1" for="star-11">1</label>
                    <input type="radio" name="star" class="star-2" id="star-12" />
                    <label class="star-2" for="star-12">2</label>
                    <input type="radio" name="star" class="star-3" id="star-13" />
                    <label class="star-3" for="star-13">3</label>
                    <input type="radio" name="star" class="star-4" id="star-14" checked />
                    <label class="star-4" for="star-14">4</label>
                    <input type="radio" name="star" class="star-5"  id="star-15"  />
                    <label class="star-5" for="star-15">5</label>
                    <span></span>
                  </div>
                </form>
                <p class="no_bottom">4 Rating</p>
              </div>
            </div>
            <div class="col-sm-8 heading_space">
              <div class="rating_progress">
                <span>Stars 5</span>
                <div class="progress">
                  <div class="progress-bar" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width:80%;"></div>
                </div>
                <span>8</span>
                <div class="clearfix"></div>
                <span>Stars 4</span>
                <div class="progress">
                  <div class="progress-bar" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width:60%;"></div>
                </div>
                <span> 6</span>
                <div class="clearfix"></div>
                <span>Stars 3</span>
                <div class="progress">
                  <div class="progress-bar" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width:40%;"></div>
                </div>
                <span> 3</span>
                <div class="clearfix"></div>
                <span>Stars 2</span>
                <div class="progress">
                  <div class="progress-bar" role="progressbar" aria-valuenow="30" aria-valuemin="0" aria-valuemax="100" style="width:30%;"></div>
                </div>
                <span>1</span>
                <div class="clearfix"></div>
                <span>Stars 1</span>
                <div class="progress">
                  <div class="progress-bar" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width:0%;"></div>
                </div>
                <span> 0</span>
              </div>
            </div>
          </div>
        </div>
        <div class="profile_border">
          <div class="profile top20 bottom20">
            <div class="p_pic"><img src="images/profile2.png" alt="Course"></div>
            <div class="profile_text">
              <h5><strong>Nikhil Trivedi</strong>  -  <span> Great for software Developers</span></h5>
              <form class="star_rating bottom5">
                <div class="stars">
                  <input type="radio" name="star" class="star-1" id="star-21" />
                  <label class="star-1" for="star-21">1</label>
                  <input type="radio" name="star" class="star-2" id="star-22" />
                  <label class="star-2" for="star-22">2</label>
                  <input type="radio" name="star" class="star-3" id="star-23" />
                  <label class="star-3" for="star-23">3</label>
                  <input type="radio" name="star" class="star-4" id="star-24"  />
                  <label class="star-4" for="star-24">4</label>
                  <input type="radio" name="star" class="star-5"  id="star-25" checked  />
                  <label class="star-5" for="star-25">5</label>
                  <span></span>
                </div>
              </form>
              <p class="margin10">Professional python developer</p>
            </div>
          </div>
        </div>
        <div class="profile_border">
          <div class="profile top20 bottom20">
            <div class="p_pic"><img src="images/profile3.png" alt="Course"></div>
            <div class="profile_text">
              <h5><strong>Varsha P</strong>  -  <span>Beginner in Programming</span></h5>
              <form class="star_rating bottom5">
                <div class="stars">
                  <input type="radio" name="star" class="star-1" id="star-31" />
                  <label class="star-1" for="star-31">1</label>
                  <input type="radio" name="star" class="star-2" id="star-32" checked />
                  <label class="star-2" for="star-32">2</label>
                  <input type="radio" name="star" class="star-3" id="star-33" />
                  <label class="star-3" for="star-33">3</label>
                  <input type="radio" name="star" class="star-4" id="star-34" checked />
                  <label class="star-4" for="star-34">4</label>
                  <input type="radio" name="star" class="star-5"  id="star-35"  />
                  <label class="star-5" for="star-35">5</label>
                  <span></span>
                </div>
              </form>
              <p class="margin10">Its fun to understand the concepts</p>
            </div>
          </div>
        </div>
        <div class="profile_border heading_space">
          <div class="profile top20 bottom20">
            <div class="p_pic"><img src="images/profile4.png" alt="Course"></div>
            <div class="profile_text">
              <h5><strong>Pankaj S</strong>  -  <span>Awesome Quality</span></h5>
              <form class="star_rating bottom5">
                <div class="stars">
                  <input type="radio" name="star" class="star-1" id="star-41" />
                  <label class="star-1" for="star-41">1</label>
                  <input type="radio" name="star" class="star-2" id="star-42" />
                  <label class="star-2" for="star-42">2</label>
                  <input type="radio" name="star" class="star-3" id="star-43" />
                  <label class="star-3" for="star-43">3</label>
                  <input type="radio" name="star" class="star-4" id="star-44" checked />
                  <label class="star-4" for="star-44">4</label>
                  <input type="radio" name="star" class="star-5"  id="star-45" />
                  <label class="star-5" for="star-45">5</label>
                  <span></span>
                </div>
              </form>
              <p class="margin10">Beginner programmer</p>
            </div>
          </div>
        </div>
        <h3>Add a Review </h3>
        <p class="heading_space">You must be <a href="#." class="logged">logged</a> in to post a comment.</p>
      </div>
      <aside class="col-sm-4 wow fadeIn" data-wow-delay="400ms">
        <div class="widget heading_space">
          <h3 class="bottom20">Featured Courses</h3>
          <div class="media">
            <a class="media-left" href="#."><img src="images/post1.png" alt="post"></a>
            <div class="media-body">
              <h5 class="bottom5">Artificial Intelligence</h5>
          <!--    <a href="#." class="btn-primary border_radius bottom5">free</a>  -->
              <form class="star_rating">
                <div class="stars">
                  <input type="radio" name="star" class="star-1" id="star-51" />
                  <label class="star-1" for="star-51">1</label>
                  <input type="radio" name="star" class="star-2" id="star-52" />
                  <label class="star-2" for="star-52">2</label>
                  <input type="radio" name="star" class="star-3" id="star-53" />
                  <label class="star-3" for="star-53">3</label>
                  <input type="radio" name="star" class="star-4" id="star-54"  />
                  <label class="star-4" for="star-54">4</label>
                  <input type="radio" name="star" class="star-5"  id="star-55" checked  />
                  <label class="star-5" for="star-55">5</label>
                  <span></span>
                </div>
              </form>
              <span class="name">Michael Windzor</span>
            </div>
          </div>
          <div class="media">
            <a class="media-left" href="#."><img src="images/post1.png" alt="post"></a>
            <div class="media-body">
              <h5 class="bottom5">Software Courses</h5>
        <!--      <a href="#." class="btn-primary border_radius bottom5">free</a> -->
              <form class="star_rating">
                <div class="stars">
                  <input type="radio" name="star" class="star-1" id="star-61" />
                  <label class="star-1" for="star-61">1</label>
                  <input type="radio" name="star" class="star-2" id="star-62" />
                  <label class="star-2" for="star-62">2</label>
                  <input type="radio" name="star" class="star-3" id="star-63" />
                  <label class="star-3" for="star-63">3</label>
                  <input type="radio" name="star" class="star-4" id="star-64"  checked />
                  <label class="star-4" for="star-64">4</label>
                  <input type="radio" name="star" class="star-5"  id="star-65"  />
                  <label class="star-5" for="star-65">5</label>
                  <span></span>
                </div>
              </form>
              <span class="name">Michael Windzor</span>
            </div>
          </div>
          <div class="media">
            <a class="media-left" href="#."><img src="images/post1.png" alt="post"></a>
            <div class="media-body">
              <h5 class="bottom5">Machine learning courses</h5>
        <!--      <a href="#." class="btn-primary border_radius bottom5">free</a>  -->
              <form class="star_rating">
                <div class="stars">
                  <input type="radio" name="star" class="star-1" id="star-71" />
                  <label class="star-1" for="star-71">1</label>
                  <input type="radio" name="star" class="star-2" id="star-72" />
                  <label class="star-2" for="star-72">2</label>
                  <input type="radio" name="star" class="star-3" id="star-73" />
                  <label class="star-3" for="star-73">3</label>
                  <input type="radio" name="star" class="star-4" id="star-74"  />
                  <label class="star-4" for="star-74">4</label>
                  <input type="radio" name="star" class="star-5"  id="star-75" checked  />
                  <label class="star-5" for="star-75">5</label>
                  <span></span>
                </div>
              </form>
              <span class="name">Michael Windzor</span>
            </div>
          </div>
        </div>
      <!--  <div class="widget heading_space">
          <h3 class="bottom20">Working Hours</h3>
          <p class="hours"> Monday <span>8:15 am - 5.30 pm</span></p>
          <p class="hours">Tuesday <span>8:15 am - 5.30 pm</span></p>
          <p class="hours">Wednesday <span>8:15 am - 5.30 pm</span></p>
          <p class="hours">Thursday <span>8:15 am - 5.30 pm</span></p>
          <p class="hours">Friday <span>8:15 am - 5.30 pm</span></p>
          <p class="hours">Saturday <span>9:30 am - 4.00 pm</span></p>
          <p class="hours">Sunday <span><a href="#." class="border_radius text-uppercase">closed</a></span></p>
        </div>
-->
        <div class="widget heading_space">
          <h3 class="bottom20">Top Tags</h3>
          <ul class="tags">
            <li><a href="#.">Books</a></li>
          <li><a href="#.">Midterm test </a></li>
            <li><a href="#.">Presentation</a></li>
            <li><a href="#.">Courses</a></li>
            <li><a href="#.">Certifications</a></li>
            <li><a href="#.">Teachers</a></li>
            <li><a href="#.">Student Life</a></li>
            <li><a href="#.">Study</a></li>
            <li><a href="#.">Midterm test </a></li>
            <li><a href="#.">Presentation</a></li>
            <li><a href="#.">Courses</a></li>
          </ul>
        </div>
      </aside>
    </div>
  </div>
</section>
<!-- Courses -->



<!--FOOTER-->
<footer class="padding-top">
  <div class="container">
    <div class="row">
      <div class="col-md-4 col-sm-4 footer_panel bottom25">
        <h3 class="heading bottom25">About Us<span class="divider-left"></span></h3>
        <a href="index.php" class="footer_logo bottom25"><img src="images/logo-white.png" alt="logo" class="logo logo-display" alt style="  width: 280px; height: 47px; ">
        <p>KnowledgeTime offers live online courses by best experts</p>
        <ul class="social_icon top25">
          <li><a href="https://www.facebook.com/knowledgetime.org/" class="facebook"><i class="fa fa-facebook"></i></a></li>
          <li><a href="https://twitter.com/knowledge_time" class="twitter"><i class="icon-twitter4"></i></a></li>
        </ul>
      </div>
      <div class="col-md-4 col-sm-4 footer_panel bottom25">
        <h3 class="heading bottom25">Quick Links<span class="divider-left"></span></h3>
        <ul class="links">
                  <li><a href="index.php"><i class="icon-chevron-small-right"></i>Home</a></li>
                  <li><a href="our_team.php"><i class="icon-chevron-small-right"></i>Our Team</a></li>
                  <li><a href="gallery.php"><i class="icon-chevron-small-right"></i>Gallery</a></li>
                <li><a href="certification.php"><i class="icon-chevron-small-right"></i>Certifications</a></li>
                  <li><a href="faq.php"><i class="icon-chevron-small-right"></i>FAQ</a></li>
                  <li><a href="privacy_policy.php"><i class="icon-chevron-small-right"></i>Privacy Policy</a></li>
        </ul>
      </div>
      <div class="col-md-4 col-sm-4 footer_panel bottom25">
        <h3 class="heading bottom25">Keep in Touch <span class="divider-left"></span></h3>
        <p class=" address"><i class="icon-map-pin"></i>Montreal, Canada</p>
        <p class=" address"><i class="icon-phone"></i>(514)-692-9467</p>
        <p class=" address"><i class="icon-mail"></i><a href="mailto:Edua@info.com">contact@knowledgetime.org</a></p>
        <img src="images/footer-map.png" alt="we are here" class="img-responsive">
      </div>
    </div>
  </div>
</footer>
<div class="copyright">
  <div class="container">
    <div class="row">
      <div class="col-md-12 text-center">
        <p>Copyright &copy; 2016 <a href="home.php">KnowledgeTime</a>. all rights reserved.</p>
      </div>
    </div>
  </div>
</div>
<!--FOOTER ends-->

<script src="js/jquery-2.2.3.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/bootsnav.js"></script>
<script src="js/jquery.appear.js"></script>
<script src="js/jquery-countTo.js"></script>
<script src="js/jquery.parallax-1.1.3.js"></script>
<script src="js/owl.carousel.min.js"></script>
<script src="js/jquery.cubeportfolio.min.js"></script>
<script src="js/jquery.themepunch.tools.min.js"></script>
<script src="js/jquery.themepunch.revolution.min.js"></script>
<script src="js/revolution.extension.layeranimation.min.js"></script>
<script src="js/revolution.extension.navigation.min.js"></script>
<script src="js/revolution.extension.parallax.min.js"></script>
<script src="js/revolution.extension.slideanims.min.js"></script>
<script src="js/revolution.extension.video.min.js"></script>
<script src="js/wow.min.js"></script>
<script src="js/functions.js"></script>

</body>
</html>
