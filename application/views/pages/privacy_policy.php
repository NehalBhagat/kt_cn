<!--Search-->
<div id="search">
  <button type="button" class="close">×</button>
  <form>
    <input type="search" value="" placeholder="Search here...."  required/>
    <button type="submit" class="btn btn_common blue">Search</button>
  </form>
</div>


<!--Page Header-->
<section class="page_header padding-top">
  <div class="container">
    <div class="row">
      <div class="col-md-12 page-content">
        <h1>Privacy Policy</h1>
        <p>KnowledgeTime offers live online courses by best experts</p>
        <div class="page_nav">
      <span>You are here:</span> <a href="<?php echo base_url();?>home/index">Home</a> <span><i class="fa fa-angle-double-right"></i>Privacy Policy</span>
      </div>
      </div>
    </div>
  </div>
</section>
<!--Page Header-->


<br>
<br>


<!--Privacy Policy-->
<div class="container">
    <div class="row">
     <div class="col-md-9 col-sm-8 wow fadeIn" data-wow-delay="400ms">
       <article class="blog_item padding-bottom-half heading_space">
         <h3>Privacy Policies of KnowledgeTime</h3>
         <p class="margin10">At KnowledgeTime, we value our user’s confidentiality and personal information. Personal information refers to your name, Email and location etc.</p>
         <p class="margin10">We may give the data of the students and their learning pattern to the experts, in order to design the course content which can benefit the students. </p>
         <p class="margin10">We may use the data of learning pattern for designing better service to the learners  </p>
         <p class="margin10">If you have any more privacy of your data concern, please contact: privacy@knowledgetime.org </p>

</div>
</div>
</div>
