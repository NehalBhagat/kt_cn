<!--Search-->
<div id="search">
  <button type="button" class="close">×</button>
  <form>
    <input type="search" value="" placeholder="Search here...."  required/>
    <button type="submit" class="btn btn_common blue">Search</button>
  </form>
</div>

<!--Page Header-->
<section class="page_header padding-top">
  <div class="container">
    <div class="row">
      <div class="col-md-12 page-content">
        <h1>Gallery Style</h1>
        <p>Education across the world</p>
        <div class="page_nav">
      <span>You are here:</span> <a href="<?php echo base_url();?>home/index">Home</a> <span><i class="fa fa-angle-double-right"></i>Gallery</span>
      </div>
      </div>
    </div>
  </div>
</section>
<!--Page Header-->



<!-- Gallery -->
<section id="gallery" class="padding">
  <div class="container">
    <div class="row">
      <div class="col-sm-5">
        <h2 class="heading heading_space">KnowledgeTime Gallery<span class="divider-left"></span></h2>
      </div>
      <div class="col-sm-7">
        <div id="project-filter" class="cbp-l-filters-alignRight">
          <div data-filter="*" class="cbp-filter-item-active cbp-filter-item">ALL IMAGES</div>
          <div data-filter=".course" class="cbp-filter-item">COURSES</div>
          <div data-filter=".book" class="cbp-filter-item">BOOKS</div>
          <div data-filter=".event" class="cbp-filter-item">EVENTS</div>
          <div data-filter=".student" class="cbp-filter-item">STUDENTS</div>
          <div data-filter=".teacher" class="cbp-filter-item">TEACHERS</div>
        </div>
      </div>
    </div>
    <div id="projects" class="cbp">
      <div class="cbp-item course">
        <img src="<?php echo base_url();?>assets/images/gallery1.jpg" alt="">
        <div class="overlay">
          <div class="centered text-center">
            <a href="<?php echo base_url();?>assets/images/gallery1.jpg" class="cbp-lightbox opens"> <i class="icon-focus"></i></a>
          </div>
        </div>
      </div>
      <div class="cbp-item course book">
        <img src="<?php echo base_url();?>assets/images/gallery2.jpg" alt="">
        <div class="overlay">
          <div class="centered text-center">
            <a href="<?php echo base_url();?>assets/images/gallery2.jpg" class="cbp-lightbox opens"> <i class="icon-focus"></i></a>
          </div>
        </div>
      </div>
      <div class="cbp-item teacher">
        <img src="<?php echo base_url();?>assets/images/gallery3.jpg" alt="">
        <div class="overlay">
          <div class="centered text-center">
            <a href="<?php echo base_url();?>assets/images/gallery3.jpg" class="cbp-lightbox opens"> <i class="icon-focus"></i></a>
          </div>
        </div>
      </div>
      <div class="cbp-item course book">
        <img src="<?php echo base_url();?>assets/images/gallery4.jpg" alt="">
        <div class="overlay">
          <div class="centered text-center">
            <a href="<?php echo base_url();?>assets/images/gallery4.jpg" class="cbp-lightbox opens"> <i class="icon-focus"></i></a>
          </div>
        </div>
      </div>
      <div class="cbp-item student event">
        <img src="<?php echo base_url();?>assets/images/gallery6.jpg" alt="">
        <div class="overlay">
          <div class="centered text-center">
            <a href="<?php echo base_url();?>assets/images/gallery6.jpg" class="cbp-lightbox opens"> <i class="icon-focus"></i></a>
          </div>
        </div>
      </div>
      <div class="cbp-item student">
        <img src="<?php echo base_url();?>assets/images/gallery8.jpg" alt="">
        <div class="overlay">
          <div class="centered text-center">
            <a href="<?php echo base_url();?>assets/images/gallery8.jpg" class="cbp-lightbox opens"> <i class="icon-focus"></i></a>
          </div>
        </div>
      </div>
      <div class="cbp-item event">
        <img src="images/gall1.jpg" alt="">
        <div class="overlay">
          <div class="centered text-center">
            <a href="<?php echo base_url();?>assets/images/gall1.jpg" class="cbp-lightbox opens"> <i class="icon-focus"></i></a>
          </div>
        </div>
      </div>
      <div class="cbp-item event">
        <img src="<?php echo base_url();?>assets/images/gallery7.jpg" alt="">
        <div class="overlay">
          <div class="centered text-center">
            <a href="<?php echo base_url();?>assets/images/gallery7.jpg" class="cbp-lightbox opens"> <i class="icon-focus"></i></a>
          </div>
        </div>
      </div>
      <div class="cbp-item student event">
        <img src="<?php echo base_url();?>assets/images/gall2.jpg" alt="">
        <div class="overlay">
          <div class="centered text-center">
            <a href="<?php echo base_url();?>assets/images/gall2.jpg" class="cbp-lightbox opens"> <i class="icon-focus"></i></a>
          </div>
        </div>
      </div>
      <div class="cbp-item teacher">
        <img src="i<?php echo base_url();?>assets/mages/gallery9.jpg" alt="">
        <div class="overlay">
          <div class="centered text-center">
            <a href="<?php echo base_url();?>assets/images/gallery9.jpg" class="cbp-lightbox opens"> <i class="icon-focus"></i></a>
          </div>
        </div>
      </div>
      <div class="cbp-item student">
        <img src="<?php echo base_url();?>assets/images/gallery13.jpg" alt="">
        <div class="overlay">
          <div class="centered text-center">
            <a href="<?php echo base_url();?>assets/images/gallery13.jpg" class="cbp-lightbox opens"> <i class="icon-focus"></i></a>
          </div>
        </div>
      </div>
      <div class="cbp-item teacher">
        <img src="<?php echo base_url();?>assets/images/gallery12.jpg" alt="">
        <div class="overlay">
          <div class="centered text-center">
            <a href="<?php echo base_url();?>assets/images/gallery12.jpg" class="cbp-lightbox opens"> <i class="icon-focus"></i></a>
          </div>
        </div>
      </div>
      <div class="cbp-item book student">
        <img src="<?php echo base_url();?>assets/images/gallery11.jpg" alt="">
        <div class="overlay">
          <div class="centered text-center">
            <a href="<?php echo base_url();?>assets/images/gallery11.jpg" class="cbp-lightbox opens"> <i class="icon-focus"></i></a>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<!-- Gallery -->
