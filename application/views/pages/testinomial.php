<?php session_start(); ?>
<!doctype html>
<html lang="en">
<body>
<?php include_once 'header.php'; ?>

<!--Search-->
<div id="search">
  <button type="button" class="close">×</button>
  <form>
    <input type="search" value="" placeholder="Search here...."  required/>
    <button type="submit" class="btn btn_common blue">Search</button>
  </form>
</div>


<!--Page Header-->
<section class="page_header padding-top">
  <div class="container">
    <div class="row">
      <div class="col-md-12 page-content">
        <h1>Testimonials</h1>
        <p>KnowledgeTime offers live online courses by best experts</p>
        <div class="page_nav">
      <span>You are here:</span> <a href="index.php">Home</a> <span><i class="fa fa-angle-double-right"></i>Testimonials</span>
      </div>
      </div>
    </div>
  </div>
</section>
<!--Page Header-->





<!--Testimonial-->
<section id="testinomila_page" class="padding">
  <div class="container">
    <div class="row">
      <div class="col-md-12 wow fadeInDown">
        <h2 class="heading heading_space">What People Say ?<span class="divider-left"></span></h2>
      </div>
    </div>
    <div id="js-grid-masonry" class="cbp">
      <div class="cbp-item">
        <div class="cbp-caption-defaultWrap">
          <div class="testimonial_wrap">
            <div class="testimonial_text text-center">
              <img src="images/quote.png" alt="quote" class="quote">
              <p>KnowledgeTime offers live online courses by best experts</p>
            </div>
            <div class="testimonial_pic">
              <img src="images/testinomial1.jpg" alt="testimonial" width="59">
              <span class="color">John Smith</span>
              <span class="post_img">Owner Edua</span>
            </div>
          </div>
        </div>
      </div>
      <div class="cbp-item">
        <div class="cbp-caption-defaultWrap">
          <div class="testimonial_wrap">
            <div class="testimonial_text text-center">
              <img src="images/quote.png" alt="quote" class="quote">
              <p>Best idea to connect all the best experts to the students </p>
            </div>
            <div class="testimonial_pic">
              <img src="images/testinomial1.jpg" alt="testimonial" width="59">
              <span class="color">John Smith</span>
              <span class="post_img">Owner Edua</span>
            </div>
          </div>
        </div>
      </div>
      <div class="cbp-item">
        <div class="cbp-caption-defaultWrap">
          <div class="testimonial_wrap">
            <div class="testimonial_text text-center">
              <img src="images/quote.png" alt="quote" class="quote">
              <p> KnowledgeTime offers live online courses by best experts
              </p>
            </div>
            <div class="testimonial_pic">
              <img src="images/testinomial1.jpg" alt="testimonial" width="59">
              <span class="color">John Smith</span>
              <span class="post_img">Owner Edua</span>
            </div>
          </div>
        </div>
      </div>
      <div class="cbp-item">
        <div class="cbp-caption-defaultWrap">
          <div class="testimonial_wrap">
            <div class="testimonial_text text-center">
              <img src="images/quote.png" alt="quote" class="quote">
              <p>KnowledgeTime offers live online courses by best experts</p>
            </div>
            <div class="testimonial_pic">
              <img src="images/testinomial1.jpg" alt="testimonial" width="59">
              <span class="color">John Smith</span>
              <span class="post_img">Owner Edua</span>
            </div>
          </div>
        </div>
      </div>
      <div class="cbp-item">
        <div class="cbp-caption-defaultWrap">
          <div class="testimonial_wrap">
            <div class="testimonial_text text-center">
              <img src="images/quote.png" alt="quote" class="quote">
              <p>KnowledgeTime offers live online courses by best experts.
              </p>
            </div>
            <div class="testimonial_pic">
              <img src="images/testinomial1.jpg" alt="testimonial" width="59">
              <span class="color">John Smith</span>
              <span class="post_img">Owner Edua</span>
            </div>
          </div>
        </div>
      </div>
      <div class="cbp-item">
        <div class="cbp-caption-defaultWrap">
          <div class="testimonial_wrap">
            <div class="testimonial_text text-center">
              <img src="images/quote.png" alt="quote" class="quote">
              <p>KnowledgeTime offers live online courses by best experts
              </p>
            </div>
            <div class="testimonial_pic">
              <img src="images/testinomial1.jpg" alt="testimonial" width="59">
              <span class="color">John Smith</span>
              <span class="post_img">Owner Edua</span>
            </div>
          </div>
        </div>
      </div>
      <div class="cbp-item">
        <div class="cbp-caption-defaultWrap">
          <div class="testimonial_wrap">
            <div class="testimonial_text text-center">
              <img src="images/quote.png" alt="quote" class="quote">
              <p>KnowledgeTime offers live online courses by best experts</p>
            </div>
            <div class="testimonial_pic">
              <img src="images/testinomial1.jpg" alt="testimonial" width="59">
              <span class="color">John Smith</span>
              <span class="post_img">Owner Edua</span>
            </div>
          </div>
        </div>
      </div>
      <div class="cbp-item">
        <div class="cbp-caption-defaultWrap testimonial_wrap">
          <div class="testimonial_text text-center">
            <img src="images/quote.png" alt="quote" class="quote">
            <p>KnowledgeTime offers live online courses by best experts</p>
          </div>
          <div class="testimonial_pic">
            <img src="images/testinomial1.jpg" alt="testimonial" width="59">
            <span class="color">John Smith</span>
            <span class="post_img">Owner Edua</span>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<!--Testimonial ends-->



<!--FOOTER-->
<?php include_once 'footer.php'; ?>
</body>
</html>
