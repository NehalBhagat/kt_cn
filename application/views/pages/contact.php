<!--Search-->
<div id="search">
  <button type="button" class="close">×</button>
  <form>
    <input type="search" value="" placeholder="Search here...."  required/>
    <button type="submit" class="btn btn_common blue">Search</button>
  </form>
</div>


<!--Page Header-->
<section class="page_header padding-top">
  <div class="container">
    <div class="row">
      <div class="col-md-12 page-content">
        <h1>Contact Us</h1>
        <p>KnowledgeTime offers live online courses by best experts</p>
        <div class="page_nav">
      <span>You are here:</span> <a href="<?php echo base_url();?>home/index">Home</a> <span><i class="fa fa-angle-double-right"></i>Contact Us</span>
      </div>
      </div>
    </div>
  </div>
</section>


<!--Contact Deatils -->
<section id="contact" class="padding">
  <div class="container">
    <div class="row padding-bottom">
      <div class="col-md-4 contact_address heading_space wow fadeInLeft" data-wow-delay="300ms">
        <h2 class="heading heading_space">Get in Touch <span class="divider-left"></span></h2>
        <p>Connect to KnowledgeTime</p>
        <div class="address">
          <i class="icon icon-map-pin border_radius"></i>
          <h4>Visit Us</h4>
          <p>Montreal, Canada.</p>
        </div>
        <div class="address">
          <i class="icon icon-mail border_radius"></i>
          <h4>Email Us</h4>
          <p><a href="mailto:Edua@info.com">contact@knowledgetime.org</a></p>
        </div>
        <div class="address">
          <i class="icon icon-phone4 border_radius"></i>
          <h4>Call Us</h4>
          <p>(+1)-514-692-9467</p>
        </div>
      </div>
      <div class="col-md-8 wow fadeInRight" data-wow-delay="300ms">
        <h2 class="heading heading_space">Fill the Contact Form<span class="divider-left"></span></h2>
        <form class="form-inline findus" id="contact-form" onSubmit="return false">
          <div class="row">
            <div class="col-md-12">
              <div id="result"></div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-4 col-sm-4">
              <div class="form-group">
                <input type="text" class="form-control" placeholder="Name"  name="name" id="name" required>
              </div>
            </div>
            <div class="col-md-4 col-sm-4">
              <div class="form-group">
                <input type="email" class="form-control" placeholder="Email" name="email" id="email" required>
              </div>
            </div>
            <div class="col-md-4 col-sm-4">
              <div class="form-group">
                <input type="text" class="form-control" placeholder="Website" name="website" id="website" required>
              </div>
            </div>
            <div class="col-md-12">
              <textarea placeholder="Comment"  name="message" id="message"></textarea>
              <button class="btn_common yellow border_radius" id="btn_submit">Submit</button>
            </div>
          </div>
        </form>
        <ul class="social_icon black top30">
          <li><a href="https://www.facebook.com/knowledgetime.org/" class="facebook"><i class="fa fa-facebook"></i></a></li>
          <li><a href="https://twitter.com/knowledge_time" class="twitter"><i class="icon-twitter4"></i></a></li>
          <li><a href="#." class="dribble"><i class="icon-dribbble5"></i></a></li>
          <li><a href="#." class="instagram"><i class="icon-instagram"></i></a></li>
        </ul>
      </div>
    </div>
    <div class="row wow bounceIn" data-wow-delay="300ms">
      <div class="col-md-12">
        <div id="map"></div>
      </div>
    </div>
  </div>
</section>
<!--Contact Deatils -->
