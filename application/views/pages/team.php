<!--Search-->
<div id="search">
  <button type="button" class="close">×</button>
  <form>
    <input type="search" value="" placeholder="Search here...."  required/>
    <button type="submit" class="btn btn_common blue">Search</button>
  </form>
</div>


<!--Page Header-->
<section class="page_header padding-top">
  <div class="container">
    <div class="row">
      <div class="col-md-12 page-content">
        <h1>Team</h1>
        <p>Meet the KnowledgeTime team</p>
        <div class="page_nav">
      <span>You are here:</span> <a href="<?php echo base_url();?>home/index">Home</a> <span><i class="fa fa-angle-double-right"></i>Our team</span>
      </div>
      </div>
    </div>
  </div>
</section>
<!--Page Header-->



<!-- Team -->
<section id="teachers" class="padding-bottom">
  <div class="container">
    <div class="row">
      <div class="col-sm-6 col-md-3">
        <div class="teacher margin_top wow fadeIn" data-wow-delay="300ms">
          <div class="image bottom25">
            <img src="<?php echo base_url();?>assets/images/team/Kiran.jpg" alt="Teachers" class=" border_radius">
            <span class="post">Founder and CTO</span>
          </div>
          <h3>Kiran Kuruvinahsetti</h3>
          <p class="bottom20 margin10">Kiran take care of executions and technology designs</p>
      <!--    <ul class="social_icon black bottom5">
            <li><a href="#." class="facebook"><i class="fa fa-facebook"></i></a></li>
            <li><a href="#." class="twitter"><i class="icon-twitter4"></i></a></li>
            <li><a href="#." class="dribble"><i class="icon-dribbble5"></i></a></li>
          </ul>  -->
        </div>
      </div>
      <div class="col-sm-6 col-md-3">
        <div class="teacher margin_top wow fadeIn" data-wow-delay="400ms">
          <div class="image bottom25">
            <img src="<?php echo base_url();?>assets/images/team/Nehal.jpg" alt="Teachers" class=" border_radius">
            <span class="post">Co-Founder and President</span>
          </div>
          <h3>Nehal Bhagat</h3>
          <p class="bottom20 margin10">Nehal works with software developments</p>
        <!--  <ul class="social_icon black bottom5">
            <li><a href="#." class="facebook"><i class="fa fa-facebook"></i></a></li>
            <li><a href="#." class="twitter"><i class="icon-twitter4"></i></a></li>
            <li><a href="#." class="dribble"><i class="icon-dribbble5"></i></a></li>
          </ul>  -->
        </div>
      </div>
      <div class="col-sm-6 col-md-3">
        <div class="teacher margin_top wow fadeIn" data-wow-delay="500ms">
          <div class="image bottom25">
            <img src="<?php echo base_url();?>assets/images/team/vinay.JPG" alt="Teachers" class=" border_radius">
            <span class="post">CEO</span>
          </div>
          <h3>Vinay Paramanand</h3>
          <p class="bottom20 margin10"> Vinay manages smooth executions in the company</p>
        <!--  <ul class="social_icon black bottom5">
            <li><a href="#." class="facebook"><i class="fa fa-facebook"></i></a></li>
            <li><a href="#." class="twitter"><i class="icon-twitter4"></i></a></li>
            <li><a href="#." class="dribble"><i class="icon-dribbble5"></i></a></li>
          </ul> -->
        </div>
      </div>
      <div class="col-sm-6 col-md-3">
        <div class="teacher margin_top wow fadeIn" data-wow-delay="600ms">
          <div class="image bottom25">
            <img src="<?php echo base_url();?>assets/images/team/Naveen.jpg" alt="Teachers" class=" border_radius">
            <span class="post">COO</span>
          </div>
          <h3>Naveen Shirur</h3>
          <p class="bottom20 margin10">Naveen works with Marketing team and Innovations</p>
      <!--    <ul class="social_icon black bottom5">
            <li><a href="#." class="facebook"><i class="fa fa-facebook"></i></a></li>
            <li><a href="#." class="twitter"><i class="icon-twitter4"></i></a></li>
            <li><a href="#." class="dribble"><i class="icon-dribbble5"></i></a></li>
          </ul>  -->
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-sm-6 col-md-3">
        <div class="teacher margin_top wow fadeIn" data-wow-delay="700ms">
          <div class="image bottom25">
            <img src="<?php echo base_url();?>assets/images/team/pankaj.jpg" alt="Teachers" class=" border_radius">
            <span class="post">Co-Founder and Lead course content designer</span>
          </div>
          <h3>Pankaj Shivhare</h3>
          <p class="bottom20 margin10">Pankaj designs all the courses</p>
    <!--      <ul class="social_icon black bottom5">
            <li><a href="#." class="facebook"><i class="fa fa-facebook"></i></a></li>
            <li><a href="#." class="twitter"><i class="icon-twitter4"></i></a></li>
            <li><a href="#." class="dribble"><i class="icon-dribbble5"></i></a></li>
          </ul>  -->
        </div>
      </div>
      <div class="col-sm-6 col-md-3">
        <div class="teacher margin_top wow fadeIn" data-wow-delay="800ms">
          <div class="image bottom25">
            <img src="<?php echo base_url();?>assets/images/team/amandeep.jpg" alt="Teachers" class=" border_radius">
            <span class="post">Co-Founder and Software Developer</span>
          </div>
          <h3>Amandeep Kaur</h3>
          <p class="bottom20 margin10">Amandeep works with software development</p>
          <ul class="social_icon black bottom5">
      <!--      <li><a href="#." class="facebook"><i class="fa fa-facebook"></i></a></li>
            <li><a href="#." class="twitter"><i class="icon-twitter4"></i></a></li>
            <li><a href="#." class="dribble"><i class="icon-dribbble5"></i></a></li>
          </ul>  -->
        </div>
      </div>
      <div class="col-sm-6 col-md-3">
        <div class="teacher margin_top wow fadeIn" data-wow-delay="900ms">
          <div class="image bottom25">
            <img src="<?php echo base_url();?>assets/images/team/lakshman.jpg" alt="Teachers" class=" border_radius">
            <span class="post">Information security Analyst</span>
          </div>
          <h3>Lakshman Pawar</h3>
          <p class="bottom20 margin10">Lakshman deals with Data security</p>
      <!--    <ul class="social_icon black bottom5">
            <li><a href="#." class="facebook"><i class="fa fa-facebook"></i></a></li>
            <li><a href="#." class="twitter"><i class="icon-twitter4"></i></a></li>
            <li><a href="#." class="dribble"><i class="icon-dribbble5"></i></a></li>
          </ul>  -->
        </div>
      </div>
      <div class="col-sm-6 col-md-3">
        <div class="teacher margin_top wow fadeIn" data-wow-delay="1000ms">
          <div class="image bottom25">
            <img src="<?php echo base_url();?>assets/images/team/natalia.jpg" alt="Teachers" class=" border_radius">
            <span class="post">Lead scientific learning officer</span>
          </div>
          <h3>Natalia Kubishyn</h3>
          <p class="bottom20 margin10">Natalia works with innovative ways of learning</p>
      <!--    <ul class="social_icon black bottom5">
            <li><a href="#." class="facebook"><i class="fa fa-facebook"></i></a></li>
            <li><a href="#." class="twitter"><i class="icon-twitter4"></i></a></li>
            <li><a href="#." class="dribble"><i class="icon-dribbble5"></i></a></li>
          </ul>  -->
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-sm-6 col-md-3">
        <div class="teacher margin_top wow fadeIn" data-wow-delay="1000ms">
          <div class="image bottom25">
            <img src="<?php echo base_url();?>assets/images/team/gaurav.jpg" alt="Teachers" class=" border_radius">
            <span class="post">Information Security Analyst</span>
          </div>
          <h3>Gaurav Gowda</h3>
          <p class="bottom20 margin10">Gaurav works with the Data Security</p>
      <!--    <ul class="social_icon black bottom5">
            <li><a href="#." class="facebook"><i class="fa fa-facebook"></i></a></li>
            <li><a href="#." class="twitter"><i class="icon-twitter4"></i></a></li>
            <li><a href="#." class="dribble"><i class="icon-dribbble5"></i></a></li>
          </ul>  -->
        </div>
      </div>
        <div class="col-sm-6 col-md-3">
          <div class="teacher margin_top wow fadeIn" data-wow-delay="1000ms">
            <div class="image bottom25">
              <img src="<?php echo base_url();?>assets/images/team/pallavi.png" alt="Teachers" class=" border_radius">
              <span class="post">Creative Writer</span>
            </div>
            <h3>Pallavi Rajpurohit</h3>
            <p class="bottom20 margin10">Pallavi lead the creative writing team</p>
      <!--      <ul class="social_icon black bottom5">
              <li><a href="#." class="facebook"><i class="fa fa-facebook"></i></a></li>
              <li><a href="#." class="twitter"><i class="icon-twitter4"></i></a></li>
              <li><a href="#." class="dribble"><i class="icon-dribbble5"></i></a></li>
            </ul>  -->
          </div>
        </div>
          <div class="col-sm-6 col-md-3">
            <div class="teacher margin_top wow fadeIn" data-wow-delay="1000ms">
              <div class="image bottom25">
                <img src="<?php echo base_url();?>assets/images/team/nandan.jpg" alt="Teachers" class=" border_radius">
                <span class="post">Front end designer</span>
              </div>
              <h3>Nandan Mrn</h3>
              <p class="bottom20 margin10">Nandan enjoys working in front end software development </p>
          <!--    <ul class="social_icon black bottom5">
                <li><a href="#." class="facebook"><i class="fa fa-facebook"></i></a></li>
                <li><a href="#." class="twitter"><i class="icon-twitter4"></i></a></li>
                <li><a href="#." class="dribble"><i class="icon-dribbble5"></i></a></li>
              </ul> -->
            </div>
          </div>
            <div class="col-sm-6 col-md-3">
              <div class="teacher margin_top wow fadeIn" data-wow-delay="1000ms">
                <div class="image bottom25">
                  <img src="<?php echo base_url();?>assets/images/team/nitin.jpg" alt="Teachers" class=" border_radius">
                  <span class="post">Finance</span>
                </div>
                <h3>Nitin Hargude</h3>
                <p class="bottom20 margin10">Nitin leads the finance team</p>
          <!--      <ul class="social_icon black bottom5">
                  <li><a href="#." class="facebook"><i class="fa fa-facebook"></i></a></li>
                  <li><a href="#." class="twitter"><i class="icon-twitter4"></i></a></li>
                  <li><a href="#." class="dribble"><i class="icon-dribbble5"></i></a></li>
                </ul>  -->
              </div>
            </div>
          </div>
          <div class="row">
              <div class="col-sm-6 col-md-3">
                <div class="teacher margin_top wow fadeIn" data-wow-delay="1000ms">
                  <div class="image bottom25">
                    <img src="<?php echo base_url();?>assets/images/team/kaveri.jpg" alt="Teachers" class=" border_radius">
                    <span class="post">Finance</span>
                  </div>
                  <h3>Kaveri Gawade</h3>
                  <p class="bottom20 margin10">Kaveri works in Finance</p>
              <!--    <ul class="social_icon black bottom5">
                    <li><a href="#." class="facebook"><i class="fa fa-facebook"></i></a></li>
                    <li><a href="#." class="twitter"><i class="icon-twitter4"></i></a></li>
                    <li><a href="#." class="dribble"><i class="icon-dribbble5"></i></a></li>
                  </ul>  -->
                </div>
              </div>
                <div class="col-sm-6 col-md-3">
                  <div class="teacher margin_top wow fadeIn" data-wow-delay="1000ms">
                    <div class="image bottom25">
                      <img src="<?php echo base_url();?>assets/images/team/neraj.png" alt="Teachers" class=" border_radius">
                      <span class="post">Software Developer</span>
                    </div>
                    <h3>Neraj Shivhare</h3>
                    <p class="bottom20 margin10">Neraj enjoys the software development</p>
                <!--    <ul class="social_icon black bottom5">
                      <li><a href="#." class="facebook"><i class="fa fa-facebook"></i></a></li>
                      <li><a href="#." class="twitter"><i class="icon-twitter4"></i></a></li>
                      <li><a href="#." class="dribble"><i class="icon-dribbble5"></i></a></li>
                    </ul> -->
                  </div>
                </div>
                  <div class="col-sm-6 col-md-3">
                    <div class="teacher margin_top wow fadeIn" data-wow-delay="1000ms">
                      <div class="image bottom25">
                        <img src="<?php echo base_url();?>assets/images/team/mrunalini.jpg" alt="Teachers" class=" border_radius">
                        <span class="post">Front end designer</span>
                      </div>
                      <h3>Mrunalini Lohar</h3>
                      <p class="bottom20 margin10">Mrunalini works with Front end design</p>
                  <!--    <ul class="social_icon black bottom5">
                        <li><a href="#." class="facebook"><i class="fa fa-facebook"></i></a></li>
                        <li><a href="#." class="twitter"><i class="icon-twitter4"></i></a></li>
                        <li><a href="#." class="dribble"><i class="icon-dribbble5"></i></a></li>
                      </ul>  -->
                    </div>
                  </div>
                </div>
  </div>
</section>
<!-- Teachers -->
