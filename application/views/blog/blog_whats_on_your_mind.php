<!--Search-->
<div id="search">
  <button type="button" class="close">×</button>
  <form>
    <input type="search" value="" placeholder="Search here...."  required/>
    <button type="submit" class="btn btn_common blue">Search</button>
  </form>
</div>


<!--Page Header-->
<section class="page_header padding-top">
  <div class="container">
    <div class="row">
      <div class="col-md-12 page-content">
        <h1>Blog</h1>
        <p>KnowledgeTime offers live online courses by best experts</p>
        <div class="page_nav">
      <span>You are here:</span> <a href="<?php echo base_url();?>home/index">Home</a> <span><i class="fa fa-angle-double-right"></i>Blog</span>
      </div>
      </div>
    </div>
  </div>
</section>

<!--BLOG SECTION-->
<section id="blog" class="padding-bottom-half padding-top">
 <h3 class="hidden">hidden</h3>
 <div class="container">
     <div class="row">
      <div class="col-md-9 col-sm-8 wow fadeIn" data-wow-delay="400ms">
        <article class="blog_item padding-bottom-half heading_space">
          <div class="image bottom25">
            <img src="<?php echo base_url();?>assets/images/blog/blogfull1.jpg" alt="blog">
          </div>
          <h3>Whats on your mind?</h3>
          <ul class="comment margin10">
            <li><a href="#.">Feb 22 2017</a></li>
            <li><a href="#."><i class="icon-comment"></i> 5</a></li>
          </ul>
          <p class="margin10">It was sometime more than a decade back, when one early morning my dad asked me get him the newspaper. Just as I opened it before giving it to him, I could see some familiar continuous words I tend to have had noticed before. When I read through them, I realized that, it was my first poem published in the newspaper. On my! What a titillating moment it was. Seeing my name out there, in one of the leading newspapers, thinking of thousands of people across geography reading it!</p>
          <p class="margin10">The words were really uncomplicated, meaning-too straight, highly intended to just bring in the rhyme. Reading it now makes me laugh. But, then that day, I realized how important a hobby to one’s life is, how it acts like a finger print differing you from the rest of the world, giving you the joy of just being yourself, being good at it, yet being passionate about bigger endeavors and that unprecedented curiosity to learn more.</p>
          <p class="margin10">Our lives today provide us minimal time, or I can rephrase it, we give the reason that our schedule gives us fewer time to do anything apart from work-home-work routine, but are we really that busy? To not even do what we enjoy doing, for at least once a week? Or is it just the laziness lingering around? I bet there are be more nods for the latter </p>
          <p class="margin10">I’ve even heard people saying they ‘plan’ to have a hobby in future, when things set right and when there is plenty of time. Well, that sounds a little illogical to me. How can a person inculcate or discover something good about himself not now, but in future? If you are a pro at something, it is never ‘a right time’, it is always ‘the right time’. It shouldn’t matter even if you are naïve, Congratulations! You’ve at least discovered what you are good at! Be it music, travelling, reading, dancing, coding (you got to be a little lucky to have this) or what not?</p>
          <p class="margin10">It’s not just exhibiting to the world about it. It is about finding inner peace and contentment, once you start doing it. That joy of feeling the hormones of positivity flowing within you, retouching your soul, with just your thoughts and ideas. Knowing yourself a little more, a little better and emerging as a refined you every time. </p>
          <p class="margin10">My mom always emphasizes on having at least one hobby. She says, when nothing stays with you in tough times, your hobby will stand by you and help you find ways through gloom. When I was a kid, the whole idea of hobby seemed too English-grammar specific. Our fourth grade grammar had –write about hobby in 150 words and I remember writing an essay on stamp collection, though I had not seen more than 4 stamps by then. But, now as I grow up, I just regret for not listening to everything she said. Moms are just always right!</p>
          <p class="margin10">Scientific studies show that, people who have creative hobbies tend to perform better at work. Their ability to control stress or help their peers is higher compared to the one with no hobby. Their ability to learn a new skill and master it takes lesser time. Mind-stimulating activities such as knitting, crochet, writing, music have been used by occupational therapists to alleviate symptoms of depression and to help improve motor functions in people with illnesses such as Parkinson’s disease. A hobby can do lot more than giving sheer joy!</p>
          <p class="margin10">So today, what’s on your mind? If these long boring lines have touched you a bit in any way, go be free, be you! Hug yourself tight, embrace yourself gently and do what you like! Because at the end, all that matters is, what you learnt and not how much you earned. All that matters is, how much did you live and not how much did you give up! If you dare not love yourself, no one else would do it for you. Love life, it has hundred reasons to love you back!</p>





        </article>
        <div class="share clearfix heading_space">
          <p class="pull-left"><strong>Share This Article:</strong></p>
          <ul class="pull-right">
            <li><a href="#."><i class="fa fa-facebook"></i></a></li>
            <li><a href="#."><i class="icon-twitter4"></i></a></li>
            <li><a href="#."><i class="icon-dribbble5"></i></a></li>
            <li><a href="#."><i class="icon-instagram"></i></a></li>
            <li><a href="#."><i class="icon-vimeo4"></i></a></li>
        </ul>
        </div>
        <div class="row">
        <div class="col-md-6">
        <article class="blog_newest text-left heading_space border_radius">
          <h2 class="hidden">Share This Article:</h2>
          <span class="post_img"><img src="<?php echo base_url();?>assets/images/post3.png" alt="newest"></span>
          <div class="text">
          <i class="link">Previous Post</i>
          <a href="#." class="post_title">Education-An Unceasing trail of changes</a>
          </div>
          </article>
        </div>
        <div class="col-md-6">
        <article class="blog_newest text-right heading_space border_radius">
          <h2 class="hidden">Share This Article:</h2>
          <div class="text">
          <i class="link">Next Post</i>
          <a href="#." class="post_title">Before Making your Dream Room that through a for market.</a>
          </div>
          <span class="post_img"><img src="<?php echo base_url();?>assets/images/post1.png" alt="newest"></span>
          </article>
        </div>
        </div>
        <article>
        <h3 class="heading bottom25">3 Comments<span class="divider-left"></span></h3>
        <div class="profile_border">
          <div class="profile top20 bottom20">
            <div class="p_pic"><img src="<?php echo base_url();?>assets/images/profile2.png" alt="instructure"></div>
            <div class="profile_text">
              <h5><strong>JOHN PARKER</strong>  -  <span> Great for Starters</span></h5>
              <ul class="comment">
                 <li><a href="#.">Jan 28, 2016 - 10:07 pm</a></li>
              </ul>
              <p>Vivamus bibendum nibh in dolor pharetra, a euismod nulla dignissim. Aenean viverra tincidunt nibh, in imperdiet nunc. Suspendisse eu ante pretium.</p>
              <a class="readmore" href="#.">LEAVE A REPLY</a>
            </div>
          </div>
        </div>
        <div class="profile_border">
          <div class="profile top20 bottom20">
            <div class="p_pic"><img src="<?php echo base_url();?>assets/images/profile3.png" alt="instructure"></div>
            <div class="profile_text">
              <h5><strong>JOHN PARKER</strong>  -  <span>Excellent Work</span></h5>
              <ul class="comment">
                 <li><a href="#.">Jan 28, 2016 - 10:07 pm</a></li>
              </ul>
              <p>Vivamus bibendum nibh in dolor pharetra, a euismod nulla dignissim. Aenean viverra tincidunt nibh, in imperdiet nunc. Suspendisse eu ante pretium.</p>
              <a class="readmore" href="#.">LEAVE A REPLY</a>
            </div>
          </div>
        </div>
        <div class="profile_border heading_space">
          <div class="profile top20 bottom20">
            <div class="p_pic"><img src="<?php echo base_url();?>assets/images/profile4.png" alt="instructure"></div>
            <div class="profile_text">
              <h5><strong>JOHN PARKER</strong>  -  <span>Awesome Quality</span></h5>
              <ul class="comment">
                 <li><a href="#.">Jan 28, 2016 - 10:07 pm</a></li>
              </ul>
              <p>Vivamus bibendum nibh in dolor pharetra, a euismod nulla dignissim. Aenean viverra tincidunt nibh, in imperdiet nunc. Suspendisse eu ante pretium.</p>
              <a class="readmore" href="#.">LEAVE A REPLY</a>
            </div>
          </div>
        </div>
        <h2 class="heading bottom25">Leave A Reply<span class="divider-left"></span></h2>
        <form class="findus heading_space">
        <div class="form-group">
          <input type="text" class="form-control" placeholder="Name" required>
        </div>
        <div class="form-group">
          <input type="email" class="form-control" placeholder="Email" required>
        </div>
        <div class="form-group">
          <input type="text" class="form-control" placeholder="Website" required>
        </div>
        <textarea placeholder="Comment"></textarea>
        <button type="submit" class="btn_common yellow border_radius">post your comment</button>
      </form>
        </article>
      </div>
      <div class="col-md-3 col-sm-4 wow fadeIn" data-wow-delay="400ms">
        <aside class="sidebar bg_grey border-radius">
          <div class="widget heading_space">
            <form class="widget_search border-radius">
              <div class="input-group">
                <input type="search" class="form-control" placeholder="Search Here" required>
                <i class="input-group-addon icon-icons185"></i>
              </div>
            </form>
          </div>
          <div class="widget heading_space">
            <h3 class="bottom20">Featured Courses</h3>
            <div class="media">
              <a class="media-left" href="#."><img src="<?php echo base_url();?>assets/images/post1.png" alt="post"></a>
              <div class="media-body">
                <h5 class="bottom5">Artificial Intelligence</h5>
                <a href="#." class="btn-primary border_radius bottom5">free</a>
                <form class="star_rating">
                  <div class="stars">
                    <input type="radio" name="star" class="star-1" id="star-1" />
                    <label class="star-1" for="star-1">1</label>
                    <input type="radio" name="star" class="star-2" id="star-2" />
                    <label class="star-2" for="star-2">2</label>
                    <input type="radio" name="star" class="star-3" id="star-3" />
                    <label class="star-3" for="star-3">3</label>
                    <input type="radio" name="star" class="star-4" id="star-4" checked  />
                    <label class="star-4" for="star-4">4</label>
                    <input type="radio" name="star" class="star-5"  id="star-5" />
                    <label class="star-5" for="star-5">5</label>
                    <span></span>
                  </div>
                </form>
                <span class="name">Michael Windzor</span>
              </div>
            </div>
            <div class="media">
              <a class="media-left" href="#."><img src="<?php echo base_url();?>assets/images/post1.png" alt="post"></a>
              <div class="media-body">
                <h5 class="bottom5">Artificial Intelligence</h5>
                <a href="#." class="btn-primary border_radius bottom5">free</a>
                <form class="star_rating">
                  <div class="stars">
                    <input type="radio" name="star" class="star-1" id="star-6" />
                    <label class="star-1" for="star-6">1</label>
                    <input type="radio" name="star" class="star-2" id="star-7" />
                    <label class="star-2" for="star-7">2</label>
                    <input type="radio" name="star" class="star-3" id="star-8" />
                    <label class="star-3" for="star-8">3</label>
                    <input type="radio" name="star" class="star-4" id="star-9"  />
                    <label class="star-4" for="star-9">4</label>
                    <input type="radio" name="star" class="star-5"  id="star-10" checked  />
                    <label class="star-5" for="star-10">5</label>
                    <span></span>
                  </div>
                </form>
                <span class="name">Michael Windzor</span>
              </div>
            </div>
            <div class="media">
              <a class="media-left" href="#."><img src="<?php echo base_url();?>assets/images/post1.png" alt="post"></a>
              <div class="media-body">
                <h5 class="bottom5">Artificial Intelligence</h5>
                <a href="#." class="btn-primary border_radius bottom5">free</a>
                <form class="star_rating">
                  <div class="stars">
                    <input type="radio" name="star" class="star-1" id="star-01" />
                    <label class="star-1" for="star-01">1</label>
                    <input type="radio" name="star" class="star-2" id="star-02" />
                    <label class="star-2" for="star-02">2</label>
                    <input type="radio" name="star" class="star-3" id="star-03" />
                    <label class="star-3" for="star-03">3</label>
                    <input type="radio" name="star" class="star-4" id="star-04"  />
                    <label class="star-4" for="star-04">4</label>
                    <input type="radio" name="star" class="star-5"  id="star-05" checked  />
                    <label class="star-5" for="star-05">5</label>
                    <span></span>
                  </div>
                </form>
                <span class="name">Michael Windzor</span>
              </div>
            </div>
          </div>
          <div class="widget heading_space">
            <h3 class="bottom20">Top Tags</h3>
            <ul class="tags">
              <li><a href="#.">Books</a></li>
              <li><a href="#.">Midterm test </a></li>
              <li><a href="#.">Presentation</a></li>
              <li><a href="#.">Courses</a></li>
              <li><a href="#.">Certifications</a></li>
              <li><a href="#.">Our team</a></li>
              <li><a href="#.">Student Life</a></li>
              <li><a href="#.">Study</a></li>
              <li><a href="#.">Midterm test </a></li>
              <li><a href="#.">Presentation</a></li>
              <li><a href="#.">Courses</a></li>
            </ul>
          </div>
        </aside>
      </div>
    </div>
  </div>
</section>
