
<?php
if (!function_exists('asset_url')) {

    function asset_url($path = "") {
        // the helper function doesn't have access to $this, so we need to get a reference to the
        // CodeIgniter instance.  We'll store that reference as $CI and use it instead of $this
        $CI = & get_instance();

        if (empty($path)) {
            return base_url() . $CI->config->item('asset_path');
        } else {
            return base_url() . $CI->config->item('asset_path') . "/" . $path;
        }
    }

}

?>
