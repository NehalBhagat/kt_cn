<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Public_CI extends CI_Controller {
  public function index()
  {

  }
  public function knowledgeTimeTeam()
  {
    $this->template->content->view('pages/team');
		$this->template->publish();
  }
  public function knowledgeTimeFaq()
  {
    $this->template->content->view('pages/faq');
		$this->template->publish();
  }
  public function knowledgeTimeGallery()
  {
    $this->template->content->view('pages/faq');
		$this->template->publish();
  }
  public function knowledgeTimeContact()
  {
    $this->template->content->view('pages/contact');
		$this->template->publish();
  }
  public function knowledgeTimeAbout()
  {
    $this->template->content->view('pages/about');
		$this->template->publish();
  }
  public function privacypolicy()
  {
    $this->template->content->view('pages/privacy_policy');
		$this->template->publish();
  }
  public function certifications()
  {
    $this->template->content->view('pages/certifications');
		$this->template->publish();
  }
  public function gallery()
  {
    $this->template->content->view('pages/gallery');
    $this->template->publish();
  }
}
  ?>
