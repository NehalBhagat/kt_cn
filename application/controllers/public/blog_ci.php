<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Blog_CI extends CI_Controller {
  public function index()
  {

  }
  public function knowledgeTimeBlog()
  {
    $this->template->content->view('blog/blog');
		$this->template->publish();
  }
  public function knowledgeTimeblogDetail1()
  {
    $this->template->content->view('blog/blog_detail');
		$this->template->publish();
  }
  public function knowledgeTimeBlogWhatsOnYourMind()
  {
    $this->template->content->view('blog/blog_whats_on_your_mind');
    $this->template->publish();
  }
  //blog_whats_on_your_mind
}
  ?>
